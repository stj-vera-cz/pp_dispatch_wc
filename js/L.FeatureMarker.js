
var L.FeatureMarker = L.Marker.extend({

    initialize: function(feature, latLng, options) {
        this.feature = feature;

        let featureLatLng = applyFeatureToLatLng(feature, latLng);
        let featureOptions = applyFeatureToOptions(feature, options);
        this.options = L.setOptions(this, featureOptions);

        // call super
        L.Marker.prototype.initialize.call(this, featureLatLng, this.options);
    },

    applyFeatureToLatLng(feature, latLng) {
        return latLng ? latLng : MapUtils.switchCoords(feature.geometry.coordinates);
    }

    applyFeatureToOptions(feature, options) {
        var options = this.options || {};

        if (feature.properties) {
            var properties = feature.properties;

            if (properties.hasOwnProperty('id')) {
                options.id = properties.id;
            }

            // pokud není ikona už ve vstupních options, vytvořit dle properties
            if (!options.icon) {
                // options pro https://github.com/lvoogdt/Leaflet.awesome-markers
                if (!!properties.color || !!properties.icon) {
                    var awsomeOptions = {};
                    if (!!properties.color) {
                        awsomeOptions.markerColor = properties.color;
                    }

                    if (!!properties.icon) {
                        awsomeOptions.prefix = 'fa';
                        awsomeOptions.icon = properties.icon;
                        if (!!properties.iconColor) {
                            awsomeOptions.iconColor = properties.iconColor;
                        }
                        if (!!properties.spin) {
                            awsomeOptions.spin = true;
                        }
                    }

                    options.icon = L.AwesomeMarkers.icon(awsomeOptions);
                }

                // options pro https://github.com/marslan390/BeautifyMarker
                if (!!properties.beautify && (!!properties.beautify.icon || !!properties.beautify.iconShape)) {
                    options.icon = L.BeautifyIcon.icon(properties.beautify);
                }
            }

            // pokud feature obsahuje v properties konfiguraci pro kontext. menu
            if (properties.contextMenu &&
                    properties.contextMenu.contextmenu === true &&
                    properties.contextMenu.contextmenuItems) {
                options.contextmenu = true;
                options.contextmenuItems = MapUtils.addCallbackToContextmenuItems(properties.contextMenu.contextmenuItems, feature, this.onContextMenuItemClickListnener);
            }
        }

        return options;
    }

});

L.featureMarker = function (feature, latlng, options) {
    return new L.FeatureMarker(feature, latlng, options);
};
