
const ICON_ADRESA_DOCASNA = L.BeautifyIcon.icon({
    icon: 'search',
    iconShape: 'marker',
    borderColor: '#ff0000',
    backgroundColor: '#ff0000',
    textColor: '#ffffff'
});

const ICON_ADRESA_STALA = L.BeautifyIcon.icon({
    icon: 'search',
    iconShape: 'marker',
    borderColor: '#ff0000',
    textColor: '#000000'
});

class VyhledavaniView {

    constructor(map) {
        this.map = map;
        this.adresaDocasnaMarker = null;
        this.adresaStalaMarker = null;
        this.predchoziInput = null;

        this.novaUdalostEventNotifier = new EventNotifier();
        this._initEmptyInputListener();
    }

    _initEmptyInputListener(listener) {
        $("#search-input").keyup(function(e) {
            if (e.target.value.length < 2) {
                $("#search-result").empty();
            }
        });
    }

    focusSearchInput() {
        $("#search-input").focus();
    }

    addOnInputChangeListener(listener) {
        $("#search-input").keyup((e) => {
            if (this.predchoziInput !== e.target.value) {
                this.predchoziInput = e.target.value;

                this.removeAdresaStalaMarker();
                this.removeAdresaDocasnaMarker();

                if (e.target.value && e.target.value.length > 1) {
                    listener(e.target.value);
                }
            }
        });
    }

    addOnNovaUdalostListener(listener) {
        this.novaUdalostEventNotifier.addListener(listener);
    }

    onVyhledavaniResult(result) {
        $("#search-result").empty();
        if (result.status === 0) {
            if (result.data) {
                if (result.data.adresy && result.data.adresy.length > 0) {
                    result.data.adresy.forEach((adresa) => {
                        $("#search-result").append(this.createResultRowDiv(adresa));
                    });
                } else {
                    $("#search-result").html("Adresa nenalezena");
                }
            }
        } else {
            $("#search-result").html(result.errmsg);
        }
    }

    createResultRowDiv(adresa) {
        let rowDiv = $("<div></div>").addClass("search-result-row");
        let infoDiv = $("<div></div>").addClass("search-result-info");
        let adrDiv = $("<div></div>").addClass("search-result-adresa");
        let adrSpan = $("<span></span>").text(adresa.adresa);
        adrDiv.append(adrSpan);
        infoDiv.append(adrDiv);
        rowDiv.append(infoDiv);

        if (adresa.latLng && adresa.latLng.lat && adresa.latLng.lng) {
            let gpsDiv = $("<div></div>").addClass("search-result-gps");
            let gpsSpan = $("<span></span>").text(adresa.latLng.lat + ', ' + adresa.latLng.lng);
            gpsDiv.append(gpsSpan);
            infoDiv.append(gpsDiv);
        }

        let addUdalost = $("<i></i>").addClass("search-add-udalost fa fa-plus-circle");
        addUdalost.click(() => {
            console.log("click " + adresa.adresa);
            this.novaUdalostEventNotifier.notifyAll(adresa);
        });
        rowDiv.append(addUdalost);

        // zobrazení markeru na místě adresy pří přejetí řádku myší
        let marker = null;
        rowDiv.mouseover(() => {
            if (adresa.latLng) {
                this.adresaDocasnaMarker = this.createAdresaMarker(adresa, ICON_ADRESA_DOCASNA);
                this.adresaDocasnaMarker.addTo(this.map);
            }
        });

        // odebrání markeru zmísta adresy pří odjetí myši z řádku
        rowDiv.mouseout(() => {
            this.removeAdresaDocasnaMarker();
        });

        // po kliku na adresu přidat stálý marker
        infoDiv.click(() => {
            this.removeAdresaStalaMarker();

            if (this.adresaDocasnaMarker) {
                this.adresaStalaMarker = this.adresaDocasnaMarker;
                this.adresaStalaMarker.setIcon(ICON_ADRESA_STALA);
                this.adresaStalaMarker.bindPopup(adresa.adresa);
                this.map.flyTo(adresa.latLng);

                this.adresaDocasnaMarker = null;
            } else if (adresa.latLng) {
                this.adresaStalaMarker = this.createAdresaMarker(adresa, ICON_ADRESA_STALA);
                this.adresaStalaMarker.bindPopup(adresa.adresa);
                this.adresaStalaMarker.addTo(this.map);
                this.map.flyTo(adresa.latLng);
            } else {
                Notify.message("Adresa neobsahuje souřadnice");
            }
        });

        return rowDiv;
    }

    removeAdresaDocasnaMarker() {
        if (this.adresaDocasnaMarker) {
            this.map.removeLayer(this.adresaDocasnaMarker);
            this.adresaDocasnaMarker = null;
        }
    }

    removeAdresaStalaMarker() {
        if (this.adresaStalaMarker) {
            this.map.removeLayer(this.adresaStalaMarker);
            this.adresaStalaMarker = null;
        }
    }

    createAdresaMarker(adresa, icon) {
        return L.marker(adresa.latLng)
            .setIcon(icon)
            .bindPopup(""); // TODO nějakej řetízek
    }
}
