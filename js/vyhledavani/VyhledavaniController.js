
class VyhledavaniController {

    constructor(map, ppClient) {
        this.ppClient = ppClient;
        this.view = new VyhledavaniView(map);

        // binding callback metod
        this.onVyhledej = this.onVyhledej.bind(this);
        this.onVyhledavaniResult = this.onVyhledavaniResult.bind(this);
        this.onNovaUdalost = this.onNovaUdalost.bind(this);

        this.view.addOnInputChangeListener(this.onVyhledej);
        this.view.addOnNovaUdalostListener(this.onNovaUdalost);
    }

    onVyhledej(input) {
        console.log("vyhledej: " + input);

        this.ppClient.vyhledejAdresy(input, this.onVyhledavaniResult);
    }

    onVyhledavaniResult(result) {
        this.view.onVyhledavaniResult(result);
    }

    onNovaUdalost(adresa) {
        this.ppClient.novaUdalost({adresaId: adresa.id, latLng: adresa.latLng});
    }

    /**
     * Callback metoda volaná z rozšíření sidebar-controllers při získání focusu na záložku Identifikace.
     */
    onFocusGained() {
        this.view.focusSearchInput();
    }
}
