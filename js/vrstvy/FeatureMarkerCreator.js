
/**
 * Vytváří obecné markery dle vlastností feature.
 */
class FeatureMarkerCreator {

    constructor(onContextMenuItemClickListnener) {
        this.onContextMenuItemClickListnener = onContextMenuItemClickListnener;
    }

    isSame(layer, feature) {
        if (layer.feature && layer.feature.properties && feature.properties) {
            return layer.feature.properties.id === feature.properties.id;
        }

        return false;
    }

    createLayer(feature) {
        let options = this._createOptions(feature);
        let coords = MapUtils.switchCoords(feature.geometry.coordinates);
        let marker = L.marker(coords, options, feature);

        if (feature.properties.popupContent) {
            marker.bindPopup(feature.properties.popupContent);
        }

        marker.feature = feature;

        return marker;
    }

    updateLayer(marker, feature) {
        let coords = MapUtils.switchCoords(feature.geometry.coordinates);
        marker.setLatLng(coords);

        if (feature.properties.popupContent) {
            marker.bindPopup(feature.properties.popupContent);
        } else {
            marker.unbindPopup();
        }

        let icon = this._creteIcon(feature);
        if (icon) {
            marker.setIcon(icon);
        }

        marker.feature = feature;
    }

    _createOptions(feature) {
        let options = {};

        options.icon = this._creteIcon(feature);

        let properties = feature.properties;

        // pokud feature obsahuje v properties konfiguraci pro kontext. menu
        if (properties &&
                properties.contextMenu &&
                properties.contextMenu.contextmenu === true &&
                properties.contextMenu.contextmenuItems) {
            options.contextmenu = true;
            options.contextmenuInheritItems = false;
            options.contextmenuItems = MapUtils.addCallbackToContextmenuItems(properties.contextMenu.contextmenuItems, feature, this.onContextMenuItemClickListnener);
        } else {
            options.contextmenu = false;
        }

        return options;
    }

    _creteIcon(feature) {
        let icon = null;

        if (feature.properties) {
            // options pro https://github.com/marslan390/BeautifyMarker
            if (!!feature.properties.beautify && (!!feature.properties.beautify.icon || !!feature.properties.beautify.iconShape)) {
                icon = L.BeautifyIcon.icon(feature.properties.beautify);
            }
        }

        return icon;
    }
}
