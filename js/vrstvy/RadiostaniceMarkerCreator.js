
const ICON_RADIOSTANICE_ONLINE = L.BeautifyIcon.icon({
    icon: 'podcast',
    iconShape: 'marker',
    borderColor: '#d9534f',
    textColor: '#000000'
});

const ICON_RADIOSTANICE_OFFLINE = L.BeautifyIcon.icon({
    icon: 'podcast',
    iconShape: 'marker',
    borderColor: '#888888',
    textColor: '#000000'
});

class RadiostaniceMarkerCreator extends TrasovaniMarkerCreator {

    _createOptions(feature) {
        let options = {};

        if (feature.properties.online) {
            options.icon = ICON_RADIOSTANICE_ONLINE;
        } else {
            options.icon = ICON_RADIOSTANICE_OFFLINE;
        }
        options.draggable = true;

        return options;
    }

    _createPopup(properties) {
        return properties.popupContent ? properties.popupContent : properties.evidencniCislo + " " + properties.celeJmeno;
    }

    _createPulsingMarker(feature, color) {
        let pulsingIcon = L.icon.pulse({iconSize:[12,12], color:color});
        let coords = MapUtils.switchCoords(feature.geometry.coordinates);
        return L.marker(coords, {icon: pulsingIcon});
    }
    
    createMajakLayer(feature) {
        if (feature.properties.aktivniNouze) {
            return this._createPulsingMarker(feature, '#CC2222');
        } else if (feature.properties.aktivniMajak) {
            return this._createPulsingMarker(feature, '#0055FF');
        }

        return null;
    }
}
