
class RadiostaniceListItemCreator {

    constructor(map) {
        this.map = map;
    }

    createItem(feature) {
        let circleColor = null;

        if (feature.properties.online) {
            circleColor = '#d9534f';
        } else {
            circleColor = '#888888';
        }
        
        return {
            name: feature.properties.evidencniCislo + " " + feature.properties.celeJmeno,
            icon: new Circle({backgroundColor: circleColor, width: '12px'}).getNode(),
            onDoubleClick: () => {
                let coords = MapUtils.switchCoords(feature.geometry.coordinates);
                this.map.flyTo(coords);
                let pulsingIcon = L.icon.pulse({iconSize:[12,12], color:'#d9534f'});
                let marker = L.marker(coords, {icon: pulsingIcon}).addTo(this.map);
                setInterval(() => {this.map.removeLayer(marker);}, 3000);
            }
        };
    }
}
