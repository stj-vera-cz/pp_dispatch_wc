
/**
 *  Kontroler pro záložku vrstvy.
 */
class VrstvyController {

    constructor(map, ppClient, options, dispatchingEventListener) {
        /**
         * Mapa
         */
        this.map = map;

        /**
         *  Klient pro volání do Genera.
         */
        this.ppClient = ppClient;

        /**
         * Event listener pro obsluhu událostí, které se mají zpracovat v jiné části dispečinku, než v záložce Vrstvy.
         */
        this.dispatchingEventListener = dispatchingEventListener;

        /**
         * Komponenta pro zapínání/vypínání zobrazení vrstev mapy.
         */
        this.layerControl = null;

        /**
         * Zjištuje zda byl přetažený marker spárován s jiným markerem v jiné vrstvě..
         */
        this.markerPairer = null;

        /**
         * Creator pro vytváření markerů trasování PP-M.
         */
        this.ppmMarkerCreator = null;

        /**
         * Creator pro vytváření markerů trasování radiostanic.
         */
        this.radiostaniceMarkerCreator = null;

        /**
         * Creator pro vytváření markerů nových událostí.
         */
        this.udalostMarkerCreator = null;

        /**
         * Creator pro vytváření markerů vyřešených událostí.
         */
        this.udalostVyresenaMarkerCreator = null;

        /**
         * Creator pro vytváření obecných markerů dle vlastností feature.
         */
        this.featureMarkerCreator = null;

        /**
         * Creator pro vytváření položek trasování PP-M do postranního panelu.
         */
        this.ppmListItemCreator = null;

        /**
         * Creator pro vytváření položek trasování radiostanic do postranního panelu.
         */
        this.radiostaniceListItemCreator = null;

        /**
         * Obsluha eventů týkajících se událostí.
         */
        this.udalostiEventHandler = null;

        // binding callback metod
        this.onRusUdalostResult = this.onRusUdalostResult.bind(this);

        // inicializace
        this._init(options)
    }

    _init(options) {
        this._initLayerControl(options.layers);
        this._initUdalostiEventHandler();
        this._initMarkerPairer();
        this._initMarkerCreators();
        this._initListItemCreators();
    }

    /**
     * Inicializace přepínače vrstev.
     */
    _initLayerControl(layers) {
        // konfigurace vrstev

        let baseLayers = this._initBaseLayers(layers.baseLayers);
        let overlays = this._initOverlays(layers.overlays);
        let externalGroup = this._initExternalOverlaysGroup(layers.externalOverlays);
        let wmsGroup = this._initWmsLayerGroup(layers.wmsLayers);

        if (!overlays) {
            overlays = [];
        }
        if (wmsGroup) {
            overlays.push(wmsGroup);
        }
        if (externalGroup) {
            overlays.push(externalGroup);
        }

        // vytvoření přepínače vrstev
        this.layerControl = new L.control.veraPanelLayers(baseLayers, overlays, {
            compact: true,
            collapsibleGroups: true,
            adjustHeight: false,
        });

        // přidání přepínače do mapy
        this.map.addControl(this.layerControl);

        // přesunutí přepínače vrstev do sidebaru
        MapUtils.changeControlParent(this.layerControl, "layer-controls");
        this.layerControl._updateHeight();
    }

    /**
     * Vytvoří vrstvy dlaždic.
     */
    _initBaseLayers(codes) {
        // vytvoření podkladových vrstev
        let baseLayers = TileLayers.createBaseLayersConfig(codes)

        // první podkladovou vrstvu rovnou přidat do mapy
        if (baseLayers && baseLayers.length > 0) {
            this.map.addLayer(baseLayers[0].layer);
        }

        return baseLayers;
    }

    /**
     * Vytvoří konfiguraci vrstev pro sidebar.
     */
    _initOverlays(configs) {
        if (configs) {
            let _this = this;
            let overlays = [];

            // při initu vytvořím prázdné LayerGroupy
            configs.forEach(function(config) {
                let cfg = null;

                // pokud přijde pouze řetězec, je to kód vrstvy, jinak celý config objekt
                if (typeof(config) == "string" || config instanceof String) {
                    cfg = {
                        "code": config,
                        "active": true
                    }
                } else {
                    cfg = config;
                    if (!cfg.hasOwnProperty("active")) {
                        cfg.active = true;
                    }
                }

                let overlay = _this._initOverlay(cfg);
                if (overlay) {
                    overlays.push(overlay);
                }
            });

            return overlays;
        }

        return null;
    }

    /**
     * Vytvoří konfiguraci vrstvy pro sidebar.
     */
    _initOverlay(config) {
        let overlay = null;

        switch (config.code) {
            case VrstvyController.Layers.TRASOVANI_PPM:
                overlay =
                    {
                        code: VrstvyController.Layers.TRASOVANI_PPM,
                        name: "Trasování mobilní aplikace",
                        active: config.active,
                        layer: L.layerGroup(),
                        subGroup: {
                            collapsed: true,
                            items:[]
                        }
                    };
                break;
            case VrstvyController.Layers.TRASOVANI_RADIO:
                overlay =
                    {
                        code: VrstvyController.Layers.TRASOVANI_RADIO,
                        name: "Trasování radiostanice",
                        active: config.active,
                        layer: L.layerGroup(),
                        subGroup: {
                            collapsed: true,
                            items:[]
                        }
                    };
                break;
            case VrstvyController.Layers.UDALOSTI:
                overlay =
                    {
                        code: VrstvyController.Layers.UDALOSTI,
                        name: "Události",
                        active: config.active,
                        layer: L.layerGroup()
                    };
                break;
            case VrstvyController.Layers.UDALOSTI_VYRESENE:
                overlay =
                    {
                        code: VrstvyController.Layers.UDALOSTI_VYRESENE,
                        name: "Vyřešené události",
                        active: config.active,
                        layer: L.layerGroup()
                    };
                break;
            default:
                // obecná vrstva markerů bez specifikcého chování, jen pro zobrazení
                if (config.layerType && config.layerType === VrstvyController.LayerTypes.FEATURE_MARKER) {
                    overlay =
                        {
                            code: config.code,
                            name: config.name,
                            layerType: config.layerType,
                            active: config.active,
                            layer: L.layerGroup()
                        };
                    break;
                }
        }

        return overlay;
    }

    /**
     * Vytvoří groupu pro externí vrstvy.
     */
    _initExternalOverlaysGroup(externalOverlays) {
        if (externalOverlays && externalOverlays.length > 0) {
            // seřadit dle abecedy
            externalOverlays.sort((a, b) => {
                if (!a.name) return 1;
                if (!b.name) return -1;

                let na = a.name.toLowerCase().normalize('NFD'),
                    nb = b.name.toLowerCase().normalize('NFD');
                if (na < nb) {
                    return -1;
                }
                if (na > nb) {
                    return 1;
                }
                return 0;
            });


            // musí obsahovat nějakou vrstvu, i když prázdnou, pak se do ní přidá vrstva geoJSONu
            externalOverlays.forEach(overlay => {
                overlay.layer = L.layerGroup();
                overlay.icon = new Rectangle({backgroundColor: overlay.color ? overlay.color : 'blue', width: '12px', height: '12px'}).getNode()
            });

            return {
                group: "Externí vrstvy",
                separator: true,
                layers: externalOverlays
            }
        }

        return null;
    }

    /**
     * Vytvoří konfiguraci pro WMS vrstvy.
     */
    _initWmsLayerGroup(wmsLayers) {
        if (wmsLayers && wmsLayers.length > 0) {
            let wmsGroup = {
                group: "WMS vrstvy",
                separator: true,
                layers: []
            }

            // seřadit dle abecedy
            wmsLayers.sort((a, b) => {
                if (!a.name) return 1;
                if (!b.name) return -1;

                let na = a.name.toLowerCase().normalize('NFD'),
                    nb = b.name.toLowerCase().normalize('NFD');
                if (na < nb) {
                    return -1;
                }
                if (na > nb) {
                    return 1;
                }
                return 0;
            });


            // vytvořit přepínač vrstvy
            wmsLayers.forEach((layer, i) => {
                //type: "tileLayer.wms", TEST betterWms respektive veraWms!!!
                let panelLayer = {
                    code: layer.code,
                    active: layer.active,
                    name: layer.name,
                    layerType: VrstvyController.LayerTypes.WMS,
                    layer: {
                        type: "tileLayer.veraWms",
					    args: [layer.url]
                    }
                }

                //vygenerovaný code pro doplnění kontext menu po vytvoření layer v přepínači L.veraPanelLayers
                if (!panelLayer.code) {
                    panelLayer.code = "cz.vera.pp.dispatch.tileLayer.wms" + i;
                }

                if (layer.options) {
                    panelLayer.layer.args.push(layer.options);
                }

                wmsGroup.layers.push(panelLayer);
            });

            return wmsGroup;
        }

        return null;
    }

    _initMarkerPairer() {
        this.markerPairer = new MarkerPairer(this.map, this.layerControl);
    }

    _initMarkerCreators() {
        this.ppmMarkerCreator = new PpmMarkerCreator(this.udalostiEventHandler, this.markerPairer, this.ppClient);
        this.radiostaniceMarkerCreator = new RadiostaniceMarkerCreator(this.udalostiEventHandler, this.markerPairer, this.ppClient);
        this.udalostMarkerCreator = new UdalostMarkerCreator(this.udalostiEventHandler, this.markerPairer, this.ppClient);
        this.udalostVyresenaMarkerCreator = new UdalostVyresenaMarkerCreator(this.udalostiEventHandler);
        this.featureMarkerCreator = new FeatureMarkerCreator(this.ppClient.onContextMenuItemClick);
    }

    _initListItemCreators() {
        this.ppmListItemCreator = new PpmListItemCreator(this.map);
        this.radiostaniceListItemCreator = new RadiostaniceListItemCreator(this.map);
    }

    _initUdalostiEventHandler() {
        let _this = this;

        this.udalostiEventHandler = {
            detailUdalosti(udalostId) {
                _this.ppClient.detailUdalosti(udalostId);
            },

            opravUdalost(udalostId) {
                _this.ppClient.opravUdalost(udalostId);
            },

            rusUdalost(udalostId) {
                _this.ppClient.rusUdalost(udalostId, result => _this.onRusUdalostResult(result, udalostId));
            },

            priradUdalostHlidce(udalostMarker, trasovaniMarker) {
                let callback = function(result) {
                    console.log(result);
                    if (result.status === 0) {
                        // TODO UPDATE marker události dle nového stavu, stav feature by měl přijít v response
                        if (result.data && result.data.udalostFeature) {
                            _this.udalostMarkerCreator.updateLayer(udalostMarker, result.data.udalostFeature);
                        }

                        Notify.message('Událost "'+ udalostMarker.feature.properties.cisloUdalosti +'" přidělena');
                    } else {
                        Notify.error(result.errmsg);
                    }
                };
                _this.ppClient.priradUdalostHlidce(udalostMarker.feature.properties.udalostId, trasovaniMarker.feature.properties.straznikId, callback);
            }
        }
    }

    onRusUdalostResult(result, udalostId) {
        if (result.status === 0) {
            Notify.message("Událost byla zrušena.");
            this.rusUdalostZVrstvy(udalostId);
        } else {
            Notify.error("Událost se nepodařilo zrušit. " + result.errmsg);
        }
    }

    rusUdalostZVrstvy(udalostId) {
        let layerDef = this.layerControl.getLayerByCode(VrstvyController.Layers.UDALOSTI);
        if (layerDef && layerDef.layer) {
            let udalostLayer = layerDef.layer.findLayer(udalostLayer => udalostLayer.feature
                                                                            && udalostLayer.feature.properties
                                                                            && udalostLayer.feature.properties.udalostId == udalostId);
            if (udalostLayer) {
                layerDef.layer.removeLayer(udalostLayer);
            }
        }
    }

    // =========================================================================
    // Metody pro nastavení features do vrstev =================================

    /**
    * Nastavení vrstvy trasování strážníků z PP-M.
    * @param geoJson GeoJSON s vrstvou trasování strážníků z PP-M
    */
    setFeaturesTrasovaniPpm(geoJson) {
        let layerDef = this.layerControl.getLayerByCode(VrstvyController.Layers.TRASOVANI_PPM);
        if (layerDef && layerDef.layer) {
            layerDef.layer.updateLayers(geoJson, this.ppmMarkerCreator);
        }

        this.updateSubGroupTrasovaniPpm(geoJson)
    }

    updateSubGroupTrasovaniPpm(geoJson) {
        let items = [];
        geoJson.features.forEach((feature) => {
            items.push(this.ppmListItemCreator.createItem(feature));
        });

        this.layerControl.updateSubGroup(VrstvyController.Layers.TRASOVANI_PPM, items);
    }

    /**
    * Nastavení vrstvy trasování strážníků z radiostanic.
    * @param geoJson GeoJSON s vrstvou trasování strážníků z radiostanic
    */
    setFeaturesTrasovaniRadio(geoJson) {
        let layerDef = this.layerControl.getLayerByCode(VrstvyController.Layers.TRASOVANI_RADIO);
        if (layerDef && layerDef.layer) {
            layerDef.layer.updateLayers(geoJson, this.radiostaniceMarkerCreator);
            this.pridejMajakyTrasovaniRadio(layerDef.layer, geoJson, this.radiostaniceMarkerCreator);
        }

        this.updateSubGroupTrasovaniRadio(geoJson);
    }

    pridejMajakyTrasovaniRadio(layerGroup, geoJson, markerCreator) {
        geoJson.features.forEach((feature) => {
            let majak = markerCreator.createMajakLayer(feature);
            if (majak) {
                layerGroup.addLayer(majak);
            }
        });
    }

    updateSubGroupTrasovaniRadio(geoJson) {
        let items = [];
        geoJson.features.forEach((feature) => {
            items.push(this.radiostaniceListItemCreator.createItem(feature));
        });

        this.layerControl.updateSubGroup(VrstvyController.Layers.TRASOVANI_RADIO, items);
    }

    /**
     * Nastavení vrstvy událostí.
     * @param geoJson GeoJSON s vrstvou událostí
     */
    setFeaturesUdalosti(geoJson) {
        let layerDef = this.layerControl.getLayerByCode(VrstvyController.Layers.UDALOSTI);
        if (layerDef && layerDef.layer) {
            layerDef.layer.updateLayers(geoJson, this.udalostMarkerCreator);
        }
    }

    /**
     * Nastavení vrstvy vyřešených událostí.
     * @param geoJson GeoJSON s vrstvou událostí
     */
    setFeaturesUdalostiVyresene(geoJson) {
        let layerDef = this.layerControl.getLayerByCode(VrstvyController.Layers.UDALOSTI_VYRESENE);
        if (layerDef && layerDef.layer) {
            layerDef.layer.updateLayers(geoJson, this.udalostVyresenaMarkerCreator);
        }
    }

    /**
     * Funkce volaná z Genera pro nastavení obecné vrstvy markerů.
     * @param code kód vrstvy
     * @param geoJson GeoJSON s vrstvou událostí
     */
    setFeaturesMarkerLayer(code, geoJson) {
        let layerDef = this.layerControl.getLayerByCode(code);
        if (layerDef && layerDef.layer) {
            layerDef.layer.updateLayers(geoJson, this.featureMarkerCreator);
        }
    }

    /**
     * Nastvení externí GeoJSON vrstvy.
     * @param code kód vrstvy
     * @param geoJson GeoJSON s objekty vrstvy
     * @param options volby nastavení
     */
    setExternalGeoJsonOverlay(code, geoJson, options) {
        let _this = this;

        let layerDef = this.layerControl.getLayerByCode(code);
        if (layerDef && layerDef.layer) {
            layerDef.layer.clearLayers();

            let layerOptions = {
                pointToLayer: function(feature, latlng) {
                      return L.circleMarker(latlng, {
                        radius: 4,
                    });
                },
                onEachFeature: function(feature, layer) {
                    // tooltip
                    if (feature.properties && feature.properties.tooltip) {
                        layer.bindTooltip(feature.properties.tooltip, {sticky: true});
                    }

                    // kontextové menu - volba pro zobrazení atributové tabulky
                    layer.bindContextMenu({
                        contextmenu: true,
                        contextmenuInheritItems: true,
                        contextmenuItems: [
                            {
                                text: 'Vlastnosti',
                                iconCls: 'fas fa-table',
                                callback: function () {
                                    _this.showGeoJsonFeatureProperties(feature, layer);
                                }
                            }
                        ]
                    });
                },
                style: {
                    color: options && options.color ? options.color : "blue"
                }
            };

            let geoLayer = L.geoJSON(geoJson, layerOptions);
            layerDef.layer.addLayer(geoLayer);
        } else {
            console.log("Vrstva '" + code + "' nebyla nalezena.");
        }
    }

    /**
     * Zobrazí atributovou tabulku (strom) GeoJSON objektu.
     *
     * @param feature GeoJSON objekt
     * @param layer   vrstva geometrie vytořená z GeoJSON ojektu
     */
    showGeoJsonFeatureProperties(feature, layer) {
        console.log('showGeoJsonFeatureProperties: ' + JSON.stringify(feature));
        if (this.dispatchingEventListener) {
            this.dispatchingEventListener.onShowProperties(feature.properties);
        }
    }

    /**
     * Zobrazí atributovou tabulku načtenou metodou getFeatureInfo aktivní WMS vrstvy.
     *
     * event event kliku na mapu, obsahuje souřadnice, které se předají jako parametr dotazu
     *
     * @param event event se souřadnicemi kliku
     */
    showWmsFeatureProperties(event) {
        let layerDefs = this.layerControl.getActiveLayersByLayerType(VrstvyController.LayerTypes.WMS);
        if (layerDefs && layerDefs.length > 0) {
            if (layerDefs.length == 1) {
                // pokud je jen jedna WMS vrstva aktivní, rovnou zavolat pro ni
                this._showWmsLayerFeatureProperties(event, layerDefs[0].layer);
            } else {
                // jinak zobrazit výběr vrstvy
                this._showWmsLayersChoice(event, layerDefs);
            }
        } else {
            if (this.dispatchingEventListener) {
                this.dispatchingEventListener.onShowPropertiesError("Není aktivní žádná WMS vrstva.");
            }
        }
    }

    /**
     * Zobrazí výběr WMS vrstvy, pro kterou se mají načítat informace metodou getFeatureInfo.
     *
     * Pro nabídku využívá (trochu prasácky, ale funguje to, lepší plugin jsem nenašel...) plugin Leaflet.contextmenu.
     * @see https://github.com/aratcliffe/Leaflet.contextmenu
     *
     * @param event event se souřadnicemi kliku
     * @param layerDefs definice aktivních WMS vrstev
     */
    _showWmsLayersChoice(event, layerDefs) {
        let _this = this;
        let _map  = this.map;

        // skrytí původních voleb context menu nad mapou
        this.map.contextmenu.hideAllItems();

        // přidání voleb pro výběr vrstvy
        let items = [];
        layerDefs.forEach((layerDef) => {
            let item = _map.contextmenu.addItem({
                text: layerDef.name,
                callback: e => {
                    this._showWmsLayerFeatureProperties(event, layerDef.layer)
                }
            });
            items.push(item);
        });

        // vyvolání nabídky
        this.map.fire('contextmenu', event);

        // po skrytí context menu s nabídkou vrstev obnovit původní podobu context menu mapy
        this.map.on('contextmenu.hide', e =>{
            // odstranění nových voleb (výběr vrstev)
            items.forEach((item) => {
                _map.contextmenu.removeItem(item);
            });

            //znovuzaktivnění původních (volby context menu nad mapou)
            _map.contextmenu.showAllItems();
        });

    }

    /**
     * Zobrazí atributovou tabulku načtenou metodou getFeatureInfo WMS vrstvy.
     *
     * @param event event se souřadnicemi kliku
     * @param layer WMS vrstva
     */
    _showWmsLayerFeatureProperties(event, layer) {
        let url = layer.getFeatureInfoUrl(event.latlng);
        this.dispatchingEventListener.onLoadWmsProperties(url);
    }
}

VrstvyController.Layers = {
        UDALOSTI: "udalosti",
        UDALOSTI_VYRESENE: "udalosti_vyresene",
        TRASOVANI_RADIO: "trasovani_radio",
        TRASOVANI_RADIO_AUTO: "trasovani_radio_auto",
        TRASOVANI_PPM: "trasovani_ppm"
    };

VrstvyController.LayerTypes = {
        FEATURE_MARKER: "feature_marker",
        WMS: "wms"
    };
