
const ICON_UDALOST_NEPRIRAZENA = L.BeautifyIcon.icon({
    icon: 'exclamation',
    iconShape: 'marker',
    borderColor: '#f0ad4e',
    textColor: '#000000'
});

const ICON_UDALOST_RESENA = L.BeautifyIcon.icon({
    icon: 'user',
    iconShape: 'marker',
    borderColor: '#0275d8',
    textColor: '#000000'
});

class UdalostMarkerCreator {

    constructor(udalostEventHandler, markerPairer, ppClient) {
        this.udalostEventHandler = udalostEventHandler;
        this.markerPairer = markerPairer;
        this.ppClient = ppClient;

        // binding pro použití "this" v callback funkcích context menu
        this._contextMenuOprav = this._contextMenuOprav.bind(this);
        this._contextMenuDetail = this._contextMenuDetail.bind(this);
        this._contextMenuRus = this._contextMenuRus.bind(this);
    }

    isSame(layer, feature) {
        if (layer.feature && layer.feature.properties && feature.properties) {
            return layer.feature.properties.udalostId === feature.properties.udalostId;
        }

        return false;
    }

    createLayer(feature) {
        let options = this._createOptions(feature);
        let coords = MapUtils.switchCoords(feature.geometry.coordinates);
        let marker = L.draggableMarker(coords, options, feature);
        let pairer = this._createPairer(marker, feature);
        marker.addOnDragendListener(pairer);

        let popup = this._createPopup(feature.properties);
        if (popup) {
            marker.bindPopup(popup);
        }

        return marker;
    }


    updateLayer(marker, feature) {
        let _this = this;

        if (marker.isDragging) {
            // odloženě provést update, jelikož pokud zrovna přetahuje marker, tak setIcon způsobí zastavení na místě
            let listener = function() {
                _this.updateLayer(marker, feature);
                marker.removeOnDragendListener(this);
            }
            marker.addOnDragendListener(listener);
        } else {
            let coords = MapUtils.switchCoords(feature.geometry.coordinates);
            marker.setLatLng(coords);

            const options = this._createOptions(feature);

            marker.setIcon(options.icon);

            // z nějakého důvodu, když není vrstva zobrazená, nemá marker property dragging, proto kontrola
            if (marker.dragging) {
                if (options.draggable) {
                    marker.dragging.enable();
                } else {
                    marker.dragging.disable();
                }
            }

            let popup = this._createPopup(feature.properties);
            if (popup) {
                marker.bindPopup(popup);
            } else {
                marker.unbindPopup();
            }

            marker.feature = feature;
        }

    }

    _createOptions(feature) {
        let options = {};

        options.draggable = true;

        if (feature.properties.prideleno) {
            options.icon = ICON_UDALOST_RESENA;
        } else {
            options.icon = ICON_UDALOST_NEPRIRAZENA;
        }

        this._createContextMenuOptions(feature, options);

        return options;
    }

    _createContextMenuOptions(feature, options) {
        options.contextmenu = true;
        options.contextmenuInheritItems = false;
        options.contextmenuItems = [
            {
                text: 'Oprav',
                index: 0,
                iconCls: "fas fa-pencil-alt",
                callback: (e) => {
                    this._contextMenuOprav(e, feature)
                }
            },
            {
                text: 'Detail',
                index: 1,
                iconCls: "fas fa-search",
                callback: (e) => {
                    this._contextMenuDetail(e, feature)
                }
            },
            {
                text: 'Ruš',
                index: 2,
                iconCls: "fas fa-trash",
                callback: (e) => {
                    this._contextMenuRus(e, feature)
                }
            }
        ]
    }

    _contextMenuOprav(e, feature) {
        this.udalostEventHandler.opravUdalost(feature.properties.udalostId);
    }

    _contextMenuDetail(e, feature) {
        this.udalostEventHandler.detailUdalosti(feature.properties.udalostId);
    }

    _contextMenuRus(e, feature) {
        /* HEG bez dotazu, dotaz zobrazí LUK v PP
        $.confirm({
            title: 'Zrušení události',
            content: 'Opravdu chcete zrušit událost?',
            type: 'orange',
            animation: 'none',
            buttons: {
                ok: () => {
                    this.udalostEventHandler.rusUdalost(feature.properties.udalostId);
                },
                storno: () => {
                    // do nothing
                }
            }
        });
        */

        this.udalostEventHandler.rusUdalost(feature.properties.udalostId);
    }

    _createPopup(properties) {
        return properties.popupContent ? properties.popupContent : null;
    }

    _createPairer(udalostMarker, feature) {
        let _this = this;

        let pairer = function(latLng) {
            if (_this.markerPairer) {
                let match = _this.markerPairer.pair(VrstvyController.Layers.TRASOVANI_PPM, latLng);
                let prirazeno = _this._priradUdalostHlidce(udalostMarker, feature, match);
                if (!prirazeno) {
                    match = _this.markerPairer.pair(VrstvyController.Layers.TRASOVANI_RADIO, latLng);
                    prirazeno = _this._priradUdalostHlidce(udalostMarker, feature, match);
                }

                return prirazeno;
            }

            return false;
        };

        return pairer;
    }

    _priradUdalostHlidce(udalostMarker, feature, match) {
        let _this = this;

        if (match && match.length > 0) {
            for (let i = 0; i < match.length; i++) {
                let trasovaniMarker = match[i];
                if (trasovaniMarker.feature
                    && trasovaniMarker.feature.properties
                    && Utils.isNotNullNum(trasovaniMarker.feature.properties.straznikId)) {
                    this.udalostEventHandler.priradUdalostHlidce(udalostMarker, trasovaniMarker);

                    return true;
                }
            }
        }

        return false;
    }
}
