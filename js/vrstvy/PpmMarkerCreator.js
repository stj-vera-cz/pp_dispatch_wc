
const ICON_PPM_STRAZNIK_ONLINE = L.BeautifyIcon.icon({
    icon: 'mobile-alt',
    iconShape: 'marker',
    borderColor: '#d9534f',
    textColor: '#000000'
});

const ICON_PPM_STRAZNIK_OFFLINE = L.BeautifyIcon.icon({
    icon: 'mobile-alt',
    iconShape: 'marker',
    borderColor: '#888888',
    textColor: '#000000'
});

class PpmMarkerCreator extends TrasovaniMarkerCreator {

    _createOptions(feature) {
        let options = {};

        if (feature.properties.iconClass) {
            options.icon = this._createMarkerIcon(feature.properties.iconClass, feature.properties.online);
        } else {
            if (feature.properties.online) {
                options.icon = ICON_PPM_STRAZNIK_ONLINE;
            } else {
                options.icon = ICON_PPM_STRAZNIK_OFFLINE;
            }
        }

        options.draggable = true;

        return options;
    }

    _createPopup(properties) {
        return properties.popupContent ? properties.popupContent : properties.evidencniCislo + " " + properties.celeJmeno;
    }

    _createMarkerIcon(iconClass, online) {
        let borderColor = online ? '#d9534f' : '#d9534f';
        return L.BeautifyIcon.icon({
            icon: iconClass,
            iconShape: 'marker',
            borderColor: borderColor,
            textColor: '#000000'
        });
    }

}
