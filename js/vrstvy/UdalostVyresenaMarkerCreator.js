
const ICON_UDALOST_VYRESENA = L.BeautifyIcon.icon({
    icon: 'check',
    iconShape: 'marker',
    borderColor: '#149414',
    textColor: '#000000'
});

class UdalostVyresenaMarkerCreator {

    constructor(udalostEventHandler) {
        this.udalostEventHandler = udalostEventHandler;

        // binding pro použití "this" v callback funkcích context menu
        this._contextMenuDetail = this._contextMenuDetail.bind(this);
    }

    isSame(layer, feature) {
        if (layer.feature && layer.feature.properties && feature.properties) {
            return layer.feature.properties.udalostId === feature.properties.udalostId;
        }

        return false;
    }

    createLayer(feature) {
        let options = this._createOptions(feature);
        let coords = MapUtils.switchCoords(feature.geometry.coordinates);
        let marker = L.marker(coords, options, feature);

        let popup = this._createPopup(feature.properties);
        if (popup) {
            marker.bindPopup(popup);
        }

        marker.feature = feature;

        return marker;
    }


    updateLayer(marker, feature) {
        let coords = MapUtils.switchCoords(feature.geometry.coordinates);
        marker.setLatLng(coords);

        let popup = this._createPopup(feature.properties);
        if (popup) {
            marker.bindPopup(popup);
        } else {
            marker.unbindPopup();
        }

        marker.feature = feature;
    }

    _createOptions(feature) {
        let options = {
            icon: ICON_UDALOST_VYRESENA
        };

        this._createContextMenuOptions(feature, options);

        return options;
    }

    _createContextMenuOptions(feature, options) {
        options.contextmenu = true;
        options.contextmenuInheritItems = false;
        options.contextmenuItems = [
            {
                text: 'Detail',
                index: 1,
                iconCls: "fas fa-search",
                callback: (e) => {
                    this._contextMenuDetail(e, feature)
                }
            }
        ]
    }

    _contextMenuDetail(e, feature) {
        this.udalostEventHandler.detailUdalosti(feature.properties.udalostId);
    }

    _createPopup(properties) {
        return properties.popupContent ? properties.popupContent : null;
    }
}
