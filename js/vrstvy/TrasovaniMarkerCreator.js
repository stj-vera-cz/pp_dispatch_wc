
class TrasovaniMarkerCreator {

    constructor(udalostEventHandler, markerPairer, ppClient) {
        this.udalostEventHandler = udalostEventHandler;
        this.markerPairer = markerPairer;
        this.ppClient = ppClient;
    }

    isSame(layer, feature) {
        if (layer.feature && layer.feature.properties && feature.properties) {
            return layer.feature.properties.straznikId === feature.properties.straznikId;
        }

        return false;
    }

    createLayer(feature) {
        let options = this._createOptions(feature);
        let coords = MapUtils.switchCoords(feature.geometry.coordinates);
        let marker = L.draggableMarker(coords, options, feature);
        let pairer = this._createPairer(marker, feature);
        marker.addOnDragendListener(pairer);

        let popup = this._createPopup(feature.properties);
        if (popup) {
            marker.bindPopup(popup);
        }

        return marker;
    }

    updateLayer(marker, feature) {
        let _this = this;

        if (marker.isDragging) {
            // odloženě provést update, jelikož pokud zrovna přetahuje marker, tak setIcon způsobí zastavení na místě
            let listener = function() {
                _this.updateLayer(marker, feature);
                marker.removeOnDragendListener(this);
            }
            marker.addOnDragendListener(listener);
        } else {
            let coords = MapUtils.switchCoords(feature.geometry.coordinates);
            marker.setLatLng(coords);

            const options = this._createOptions(feature);

            marker.setIcon(options.icon);

            // z nějakého důvodu, když není vrstva zobrazená, nemá marker property dragging, proto kontrola
            if (marker.dragging) {
                if (options.draggable) {
                    marker.dragging.enable();
                } else {
                    marker.dragging.disable();
                }
            }

            let popup = this._createPopup(feature.properties);
            if (popup) {
                marker.bindPopup(popup);
            } else {
                marker.unbindPopup();
            }

            marker.feature = feature;
        }

    }

    _createPairer(trasovaniMarker, feature) {
        let _this = this;

        let pairer = function(latLng) {
            if (_this.markerPairer) {
                let match = _this.markerPairer.pair(VrstvyController.Layers.UDALOSTI, latLng);
                if (match && match.length > 0) {
                    for (let i = 0; i < match.length; i++) {
                        let udalostMarker = match[i];
                        if (udalostMarker.feature && udalostMarker.feature.properties && Utils.isNotNullNum(feature.properties.straznikId)) {
                            _this.udalostEventHandler.priradUdalostHlidce(udalostMarker, trasovaniMarker);

                            return true;
                        }
                    }
                }
            }

            return false;
        };

        return pairer;
    }
}
