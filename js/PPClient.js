const ACTION_NOVA_UDALOST = "nova_udalost";
const ACTION_OPRAV_UDALOST = "oprav_udalost";
const ACTION_DETAIL_UDALOSTI = "detail_udalosti";
const ACTION_RUS_UDALOST = "rus_udalost";
const ACTION_PRIRAD_UDALOST_HLIDCE = "prirad_udalost_hlidce";
const ACTION_IDENTIFIKACE = "identifikace";
const ACTION_VYHLEDEJ_ADRESY = "vyhledej_adresy";
const ACTION_WMS_GET_FEATURE_INFO = "wms_get_feature_info";

/**
 * Klient pro zasílání akcí do PP.
 */
class PPClient {

    constructor() {
        this.callbackStorage = new ActionCallbackStorage();
    }

    /**
     * Vyvolá akci založení nové události.
     * @param misto místo nové události, obsahuje buď latLng souřadnice nebo adresaId.
     */
    novaUdalost(misto) {
        execAction(ACTION_NOVA_UDALOST, misto);
    }

    /**
     * Vyvolá akci pro opravu události.
     * @param udalostId id události
     */
    opravUdalost(udalostId) {
        execAction(ACTION_OPRAV_UDALOST, udalostId);
    }

    /**
     * Vyvolá akci pro zobrazení detailu události.
     * @param udalostId id události
     */
    detailUdalosti(udalostId) {
        execAction(ACTION_DETAIL_UDALOSTI, udalostId);
    }

    /**
     * Vyvolá akci pro rušení události.
     * @param udalostId id události
     * @param resultCallback callback funkce s výsledkem rušení
     */
    rusUdalost(udalostId, resultCallback) {
        let actionId = this.callbackStorage.addActionCallback(resultCallback);
        let actionData = {
            actionId: actionId,
            udalostId: udalostId
        };
        execAction(ACTION_RUS_UDALOST, actionData);
    }

    /**
     * Vyvolá akci přiřazení události hlídce ve které je zařazen vybraný strážník.
     *
     * @param udalostId id události
     * @param straznikId id strážníka, ktetému má být událost přiřazena
     * @param resultCallback callback funkce s výsledkem přiřazení
     */
    priradUdalostHlidce(udalostId, straznikId, resultCallback) {
        let actionId = this.callbackStorage.addActionCallback(resultCallback);
        let actionData = {
            actionId: actionId,
            udalostId: udalostId,
            straznikId: straznikId
        };
        execAction(ACTION_PRIRAD_UDALOST_HLIDCE, actionData);
    }

    /**
     * Vyvolá akci pro identifikaci místa na zadaných souřadnicích.
     *
     * @param latLng souřadnice místa
     * @param resultCallback callback funkce s výsledkem identifikace
     */
    identifikace(latLng, resultCallback) {
        let actionId = this.callbackStorage.addActionCallback(resultCallback);
        let actionData = {
            actionId: actionId,
            latLng: latLng
        };
        execAction(ACTION_IDENTIFIKACE, actionData);
    }

    /**
     * Vyvolá akci pro vyhledání adres dle vstupního řetězce.
     *
     * @param input vyhledávaný řetězec
     * @param resultCallback callback funkce s výsledkem vyhledání adres
     */
    vyhledejAdresy(input, resultCallback) {
        let actionId = this.callbackStorage.addActionCallback(resultCallback);
        let actionData = {
            actionId: actionId,
            input: input
        };
        execAction(ACTION_VYHLEDEJ_ADRESY, actionData);
    }

    /**
     * Vyvolá akci pro získání informací o WMS vrstvě na souřadnicích zadaných v url.
     *
     * @param url url dotazu
     * @param resultCallback callback funkce vyvolaná po skončení dotazu
     */
    wmsGetFeatureInfo(url, resultCallback) {
        let actionId = this.callbackStorage.addActionCallback(resultCallback);
        let actionData = {
            actionId: actionId,
            url: url
        };
        execAction(ACTION_WMS_GET_FEATURE_INFO, actionData);
    }

    /**
     *  Vyvolá akci po kliku na kontextové menu nějaké reprezentace feature (markeru, geometrie).
     *
     * @param action akce kontextového menu
     * @param event  data { action, latLng, feature }
     */
    onContextMenuItemClick(action, event) {
        execAction(action, event);
    }

    /**
     * Metoda volaná z PP pro předání výsledku akce zpět do PPDispatching.
     * @param actionId id akce
     * @param result data výsledku operace
     */
    actionResult(actionId, result) {
        console.log(actionId);
        console.log(result);
        this.callbackStorage.callActionCallback(actionId, result);
    }
}
