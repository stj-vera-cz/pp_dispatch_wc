
class IdentifikaceController {

    constructor(map, ppClient) {
        this.ppClient = ppClient;

        this.view = new IdentifikaceView(map);
        this.identifyMode = new IdentifyMode(map);

        this.identifyPosition = this.identifyPosition.bind(this);
        this.identifyPositionResult = this.identifyPositionResult.bind(this);

        this.identifyMode.addIdentifyListener(this.identifyPosition);
    }

    identifyPosition(latLng) {
        this.view.onIdentifyPosition(latLng);
        this.ppClient.identifikace(latLng, this.identifyPositionResult);
    }

    identifyPositionResult(result) {
        this.view.onIdentifyPositionResult(result)
    }

    /**
     * Callback metoda volaná z rozšíření sidebar-controllers při získání focusu na záložku Identifikace.
     */
    onFocusGained() {
        this.identifyMode.on();
    }

    /**
     * Callback metoda volaná z rozšíření sidebar-controllers při přepnutí ze záložky Identifikace na jinou.
     */
    onFocusLost() {
        this.identifyMode.off();
    }
}
