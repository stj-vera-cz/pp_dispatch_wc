
/**
 * Zapíná/vypíná identifikační mód nad mapou.
 */
class IdentifyMode {

    constructor(map) {
        this.map = map;

        this.active = false;
        this.identifyListener = null;

        let _this = this;
        this.onClick = function(e) {
            if (_this.identifyListener) {
                _this.identifyListener(e.latlng);
            }
        }
    }

    addIdentifyListener(listener) {
        this.identifyListener = listener;
    }

    on() {
        this.active = true;
        this.map.on('click', this.onClick);
        this.map._container.style.cursor = "help";
    }

    off() {
        this.active = false;
        this.map.off('click', this.onClick);
        this.map._container.style.cursor = null;
    }

}
