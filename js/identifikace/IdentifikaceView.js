
/**
 * Modifikace View záložky Identifikace.
 */
class IdentifikaceView {

    constructor(map) {
        this.map = map;
        this.geometryLayer = null;

        let _this = this;
        $('#identify-clear-btn').click(function(){
            _this.clear();
        });
    }

    clear() {
        $("#identify-latlng").val(null);
        $("#identify-result").html(null);
        $("#identify-error").html(null);
        this.removeGeometry();
    }

    /**
     * Metoda vyvolaná na začátku identifikace, kdy jsou známy souřadnice místa.
     */
    onIdentifyPosition(latLng) {
        this.clear();
        $("#identify-latlng").val(MapUtils.latLngToString(latLng));
        $("#identify-spinner").show();
    }

    /**
     * Metoda vyvolaná po přijetí výsledku identifikace.
     */
    onIdentifyPositionResult(result) {
        $("#identify-spinner").hide();

        if (result && result.data) {
            this.onIsvrResult(result.data.isvr);
            this.onIsknResult(result.data.iskn);
            this.onGeometryResult(result.data.geometry);
        } else {
            $("#identify-error").append($("<div></div>").text("Odpověď neobsahuje data!").addClass("form-error"));
        }
    }

    onIsvrResult(isvr) {
        if (isvr) {
            if (isvr.status === 0) {
                if (isvr.data) {
                    this.createIsvrFormFields(isvr.data);
                }
            } else {
                $("#identify-error").append($("<div></div>").text(isvr.errmsg).addClass("form-error"));
            }
        }
    }

    onIsknResult(iskn) {
        if (iskn) {
            if (iskn.status === 0) {
                if (iskn.data) {
                    this.createIsknFormFields(iskn.data);
                }
            } else {
                $("#identify-error").append($("<div></div>").text(iskn.errmsg).addClass("form-error"));
            }
        }
    }

    onGeometryResult(geometry) {
        if (geometry) {
            if (geometry.status === 0) {
                if (geometry.data) {
                    this.drawGeometry(geometry.data);
                }
            } else {
                $("#identify-error").append($("<div></div>").text(geometry.errmsg).addClass("form-error"));
            }
        }
    }

    createIsvrFormFields(data) {
        let formDiv = $("#identify-result");

        if (data.parId) {
            formDiv.append(this.createFieldDiv("ID parcely", data.parId));
        }
        if (data.cislo) {
            formDiv.append(this.createFieldDiv("Číslo parcely", data.cislo));
        }
        if (data.cisloLV) {
            formDiv.append(this.createFieldDiv("Číslo LV", data.cisloLV));
        }
        if (data.zpusobVyuziti) {
            formDiv.append(this.createFieldDiv("Způsob využití", data.zpusobVyuziti));
        }
        if (data.druhPozemku) {
            formDiv.append(this.createFieldDiv("Druh pozemku", data.druhPozemku));
        }
    }

    createIsknFormFields(data) {
        let _this = this;

        let formDiv = $("#identify-result");

        formDiv.append($("<div></div>").text("Vlastníci").addClass("form-label"));
        if (data && data.Vlastnik && data.Vlastnik.length > 0) {
            data.Vlastnik.forEach((vlastnik, i) => {
                let vlastnikDiv = _this.createVlastnikRow(vlastnik);
                if (vlastnikDiv) {
                    formDiv.append(vlastnikDiv);
                    formDiv.append($("<hr />"));
                }
            });
        } else {
            formDiv.append($("<div></div>").text("Vlastníky se nepodařilo identifikovat."));
        }
    }

    drawGeometry(geometry) {
        this.geometryLayer = MapUtils.addGeoJsonGeometry(this.map, geometry, null, false);
    }

    removeGeometry() {
        if (this.geometryLayer) {
            this.map.removeLayer(this.geometryLayer);
            this.geometryLayer = null;
        }
    }

    createFieldDiv(label, value) {
        let fieldDiv = $("<div></div>").addClass("form-field");

        fieldDiv.append($("<div></div>").text(label).addClass("form-label"));
        fieldDiv.append($("<div></div>").text(value).addClass("form-value"));

        return fieldDiv;
    }

    createVlastnikRow(vlastnik) {
        let rowDiv = $("<div></div>").addClass("identifikace-vlastnik-row");

        if (vlastnik.FyzickaOsoba) {
            rowDiv.append($("<div><i>Fyzická osoba</i></div>").addClass("form-field"));
            this.createFyzickaOsoba(rowDiv, vlastnik.FyzickaOsoba);
        } else if (vlastnik.PravnickaOsoba) {
            rowDiv.append($("<div><i>Právnická osoba</i></div>").addClass("form-field"));
            rowDiv.append($("<div></div>").text(vlastnik.PravnickaOsoba.Nazev).addClass("form-field"));

            let adresa = this.createAdresaString(vlastnik.PravnickaOsoba.Adresa);
            rowDiv.append($("<div></div>").text(adresa).addClass("form-field"));
        } else if (vlastnik.SpolecneJmeniManzelu) {
            rowDiv.append($("<div><i>Společné jmění manželů</i></div>").addClass("form-field"));
            rowDiv.append($("<div></div>").text(vlastnik.SpolecneJmeniManzelu.Nazev).addClass("form-field"));

            this.createFyzickaOsoba(rowDiv, vlastnik.SpolecneJmeniManzelu.Partner1);
            this.createFyzickaOsoba(rowDiv, vlastnik.SpolecneJmeniManzelu.Partner2);
        }

        return rowDiv;
    }

    createFyzickaOsoba(rowDiv, fyzickaOsoba) {
        let jmeno = fyzickaOsoba.Prijmeni + " " + fyzickaOsoba.Jmeno;
        rowDiv.append($("<div></div>").text(jmeno).addClass("form-field"));

        let adresa = this.createAdresaString(fyzickaOsoba.Adresa);
        rowDiv.append($("<div></div>").text(adresa).addClass("form-field"));
    }

    createAdresaString(adresa) {
        let adr = "";

        if (adresa) {
            if (adresa.Ulice) {
                adr = adr.concat(adresa.Ulice);
            }
            if (adresa.CisloDomovni) {
                if (adr.length > 0) {
                    adr = adr.concat(" ");
                }
                adr = adr.concat(adresa.CisloDomovni);
            }
            if (adresa.CisloOrientacni) {
                if (adr.length > 0) {
                    if (adresa.CisloDomovni) {
                        adr = adr.concat("/");
                    } else {
                        adr = adr.concat(" ");
                    }
                }
                adr = adr.concat(adresa.CisloOrientacni);
            }
            if (adresa.CastObce && adresa.CastObce != adresa.Obec ) {
                if (adr.length > 0) {
                    adr = adr.concat(", ");
                }
                adr = adr.concat(adresa.CastObce);
            }
            if (adresa.Obec) {
                if (adr.length > 0) {
                    adr = adr.concat(", ");
                }
                adr = adr.concat(adresa.Obec);
            }

            return adr;
        }
    }

}
