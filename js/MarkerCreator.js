
class MarkerCreator {

    constructor(feature, options) {
        this.feature = feature;
        this.options = options;
    }

    create() {
        var coords = feature.geometry.coordinates;

        var options = this.createDefaultMarkerOptions(feature);
        var marker = L.marker([coords[1], coords[0]], options);

        if (feature.properties && !!feature.properties.popupContent) {
            marker.bindPopup(feature.properties.popupContent);
        }

        return marker;
    }

    /**
     * Vytvoří options pro značku(marker).
     *
     * @see https://leafletjs.com/reference-1.4.0.html#marker-option
     * @see https://github.com/lvoogdt/Leaflet.awesome-markers
     *
     * @param feature
     * @returns options
     *
     */
    createDefaultMarkerOptions(feature) {
        var options = this.options || {};

        if (feature.properties) {
            var properties = feature.properties;

            if (properties.hasOwnProperty('id')) {
                options.id = properties.id;
            }

            // pokud není ikona už ve vstupních options, vytvořit dle properties
            if (!options.icon) {
                // options pro https://github.com/lvoogdt/Leaflet.awesome-markers
                if (!!properties.color || !!properties.icon) {
                    var awsomeOptions = {};
                    if (!!properties.color) {
                        awsomeOptions.markerColor = properties.color;
                    }

                    if (!!properties.icon) {
                        awsomeOptions.prefix = 'fa';
                        awsomeOptions.icon = properties.icon;
                        if (!!properties.iconColor) {
                            awsomeOptions.iconColor = properties.iconColor;
                        }
                        if (!!properties.spin) {
                            awsomeOptions.spin = true;
                        }
                    }

                    options.icon = L.AwesomeMarkers.icon(awsomeOptions);
                }

                // options pro https://github.com/marslan390/BeautifyMarker
                if (!!properties.beautify && (!!properties.beautify.icon || !!properties.beautify.iconShape)) {
                    options.icon = L.BeautifyIcon.icon(properties.beautify);
                }
            }

            // pokud feature obsahuje v properties konfiguraci pro kontext. menu
            if (properties.contextMenu &&
                    properties.contextMenu.contextmenu === true &&
                    properties.contextMenu.contextmenuItems) {
                options.contextmenu = true;
                options.contextmenuItems = MapUtils.addCallbackToContextmenuItems(properties.contextMenu.contextmenuItems, feature, this.onContextMenuItemClickListnener);
            }
        }

        return options;
    }
}
