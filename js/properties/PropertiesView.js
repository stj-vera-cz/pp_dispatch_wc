

/**
 * View záložky Vlastnosti.
 */
class PropertiesView {

    constructor() {
    }

    clear() {
        this._setPropertiesHtml(null);
        this._setErrorHtml(null);
    }

    loadingStarted() {
        this.clear();
        this.startSpinner();
    }

    startSpinner() {
        $("#properties-spinner").show();
    }

    stopSpinner() {
        $("#properties-spinner").hide();
    }

    showProperties(properties) {
        this.clear();
        this.stopSpinner();

        if (properties) {
            if (typeof properties === 'string') {
                this._setPropertiesHtml(properties);
            } else {
                if (Object.keys(properties).length > 0) {
                    this._createPropertyTable(properties);
                } else {
                    this._setErrorHtml("Žádné vlastnosti k zobrazení");
                }
            }
        } else {
            this._setErrorHtml("Žádné vlastnosti k zobrazení");
        }
    }

    showError(error) {
        this.clear();
        this.stopSpinner();
        this._setErrorHtml(error);
    }

    _createPropertyTable(properties) {
        new PropertyTable('properties-tree', properties);
    }

    _setPropertiesHtml(properties) {
        $("#properties-tree").html(properties);
    }

    _setErrorHtml(error) {
        $("#properties-error").html(error);
    }
}
