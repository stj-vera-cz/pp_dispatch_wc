
/**
 * Controller záložky Vlastnosti.
 */
class PropertiesController {

    constructor(ppClient) {
        this.ppClient = ppClient;

        this.view = new PropertiesView();
    }

    onPropertiesLoadingStarted() {
        this.view.loadingStarted();
    }

    onShowProperties(properties) {
        this.view.showProperties(properties);
    }

    onShowPropertiesError(error) {
        this.view.showError(error);
    }

    onLoadWmsProperties(url) {
        let _this = this;

        this.onPropertiesLoadingStarted();
        this.ppClient.wmsGetFeatureInfo(url, result => {
            console.log("wmsGetFeatureInfo result status: " + result.status);
            if (result.status === 0) {
                _this.onShowProperties(result.data ? result.data.info : null);
            } else {
                _this.onShowPropertiesError("Chyba zjištování vlastností WMS vrstvy:<br />" + result.errmsg);
            }
        });
    }
}
