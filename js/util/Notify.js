
/**
 * Notifikace uživatele.
 */
class Notify {

    static message(msg) {
        Utils.toast(msg);
    }

    static error(msg) {
        Utils.toastError(msg);
    }
}
