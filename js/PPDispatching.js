
/**
 * Hlavní třída dispečinku PP.
 */
class PPDispatching {

    constructor(mapId, options, ppClient) {
        /**
         * Id html elementu na který má být navěšena mapa.
         */
        this.mapId = mapId;

        /**
         * Parametry mapy.
         */
        this.options = options || {};

        /**
         * Klient pro zasílání akcí do PP.
         */
        this.ppClient = ppClient;

        /**
         * Leaflet mapa.
         */
        this.map = null;

        /**
         * Postranní lišta.
         */
        this.sidebar = null;

        /**
         * Controller záložky Vrstvy.
         */
        this.vrstvyController = null;

        /**
         * Controller záložky Vyhledávání.
         */
        this.vyhledavaniController = null;

        /**
         * Controller záložky Identifikace.
         */
        this.identifikaceController = null;

        /**
         * Controller záložky Vlastnosti
         */
        this.propertiesController = null;

        // inicializace
        this._init(options);
    }

    /**
     * Prvotní inicializace mapy.
     */
     _init(options) {
        this._initMap(options);
        this._initTools(options);
        this._initControllers(options);
        this._initSidebar(options);
    }

    /**
     * Vytvoří instanci Leafletjs mapy.
     */
    _initMap(options) {
        // výchozí souřadnice
        let mapOptions = options.map || MapUtils.DEFAULT_MAP_OPTIONS_CZECH_REPUBLIC;
        if (!mapOptions.center || !Array.isArray(mapOptions.center) || mapOptions.center < 2) {
            mapOptions.center = MapUtils.DEFAULT_MAP_OPTIONS_CZECH_REPUBLIC.center;
            mapOptions.zoom = 8;
        }
        if (mapOptions.zoom == null) {
            mapOptions.zoom = MapUtils.DEFAULT_MAP_OPTIONS_CZECH_REPUBLIC.zoom;
        }

        mapOptions.crs = L.CRS.EPSG3857;

        // kontextové menu
        // nutno bindovat metody volané z kontextového menu, aby měly přístup k PPDispatching přes "this".
        this.contextMenuEventNovaUdalost = this.contextMenuEventNovaUdalost.bind(this);
        this.contextMenuEventIdentifikuj = this.contextMenuEventIdentifikuj.bind(this);
        this.contextMenuEventShowCoordinates = this.contextMenuEventShowCoordinates.bind(this);
        this.contextMenuEventVlastnostiWms = this.contextMenuEventVlastnostiWms.bind(this);

        mapOptions.contextmenu = true; // Musí být true, aby fungovalo skrývání kontextového menu.
        //mapOptions.contextmenuWidth = 140;
        mapOptions.contextmenuItems = [
            {
                text: "Nová událost",
                iconCls: "fas fa-file",
                callback: this.contextMenuEventNovaUdalost
            },
            {
                text: "Identifikuj",
                iconCls: "fas fa-question",
                callback: this.contextMenuEventIdentifikuj
            },
            {
                text: "Souřadnice",
                iconCls: "fas fa-map-marker-alt",
                callback: this.contextMenuEventShowCoordinates
            }
        ];

        // pokud jsou nějaké WMS vrstvy, umožnit zobrazit Vlastnosti
        if (options.layers
            && options.layers.wmsLayers
            && options.layers.wmsLayers) {
            mapOptions.contextmenuItems.push({
                text: 'Vlastnosti WMS',
                iconCls: 'fas fa-table',
                callback: this.contextMenuEventVlastnostiWms
            })
        }

        // vytvoření mapy
        this.map = L.map(this.mapId, mapOptions);

        // měřítko do mapy
        L.control.scale({
            maxWidth: 200,
            metric: true,
            imperial: false
        }).addTo(this.map);
    }

    /**
     * Inicializace pomocných nástrojů.
     * @param options
     */
    _initTools(options) {
        // tlačítko pro reset mapy do výchozí pozice

        // změna na ikonu FA5
        //L.easyButton('fa-dot-circle-o', function(btn, map){
        L.easyButton('far fa-dot-circle', function(btn, map){
            map.setView(options.map.center, options.map.zoom);
        }, 'Výchozí pozice').addTo(this.map);
    }

    /**
     * Inicializace controllerů.
     * @param options
     */
    _initControllers(options) {
        this.vrstvyController = new VrstvyController(this.map, this.ppClient, options, this);

        if (options.search && options.search.active === true) {
            this.vyhledavaniController = new VyhledavaniController(this.map, this.ppClient);
        }

        this.identifikaceController = new IdentifikaceController(this.map, this.ppClient);

        this.propertiesController = new PropertiesController(this.ppClient);
    }

    /**
     * Inicializace sidebaru.
     */
    _initSidebar(options) {
        this.sidebar = L.control.sidebar({
            autopan: false,
            closeButton: true,
            container: 'sidebar',
            position: 'right'
        }).addTo(this.map);

        this.sidebar.registerController('identify-pane', this.identifikaceController);

        this.sidebar.registerController('search-pane', this.vyhledavaniController);

        this.sidebar.open('layers-pane');

        if (!options.search || options.search.active !== true) {
            this.sidebar.removePanel('search-pane');
        }
    }

    /**
     *  Přepne se na požadovanou záložku sidebaru, pokud již neni otevřená.
     */
    openSidebarPanel(id) {
        if (!this.sidebar.isPanelOpened(id)) {
            this.sidebar.open(id);
        }
    }

    /**
     *  Přepne se na záložku "Identifikace" sidebaru, pokud již neni otevřená.
     */
    openIdentifyPanel() {
        this.openSidebarPanel('identify-pane');
    }

    /**
     *  Přepne se na záložku "Vlastnosti" sidebaru, pokud již neni otevřená.
     */
    openPropertiesPanel() {
        this.openSidebarPanel('properties-pane');
    }

    // =========================================================================
    // Metody pro obsluhu akcí kontextového menu nad mapou =====================

    contextMenuEventNovaUdalost(e) {
        this.ppClient.novaUdalost({latLng: e.latlng});
    }

    contextMenuEventIdentifikuj(e) {
        this.openIdentifyPanel();
        this.identifikaceController.identifyPosition(e.latlng);
    }

    contextMenuEventShowCoordinates(e) {
        L.popup()
            .setLatLng(e.latlng)
            .setContent(MapUtils.latLngToString(e.latlng))
            .openOn(this.map);
    }

    contextMenuEventVlastnostiWms(e) {
        this.vrstvyController.showWmsFeatureProperties(e)
    }

    // =========================================================================
    // Metody pro nastavení features do vrstev =================================

    /**
     * Funkce pro nastavení vrstvy trasování strážníků z PP-M.
     * @param geoJson GeoJSON s vrstvou trasování strážníků z PP-M
     */
    setFeaturesTrasovaniPpm(geoJson) {
        this.vrstvyController.setFeaturesTrasovaniPpm(geoJson);
    }

    /**
     * Funkce pro nastavení vrstvy trasování strážníků z radiostanic.
     * @param geoJson GeoJSON s vrstvou trasování strážníků z radiostanic
     */
    setFeaturesTrasovaniRadio(geoJson) {
        this.vrstvyController.setFeaturesTrasovaniRadio(geoJson);
    }

    /**
     * Funkce pro nastavení vrstvy událostí.
     * @param geoJson GeoJSON s vrstvou událostí
     */
    setFeaturesUdalosti(geoJson) {
        this.vrstvyController.setFeaturesUdalosti(geoJson);
    }

    /**
     * Funkce pro nastavení vrstvy vyřešených událostí.
     * @param geoJson GeoJSON s vrstvou událostí
     */
    setFeaturesUdalostiVyresene(geoJson) {
        this.vrstvyController.setFeaturesUdalostiVyresene(geoJson);
    }

    /**
     * Funkce pro nastavení obecné vrstvy markerů.
     * @param code kód vrstvy
     * @param geoJson GeoJSON s vrstvou událostí
     */
    setFeaturesMarkerLayer(code, geoJson) {
        this.vrstvyController.setFeaturesMarkerLayer(code, geoJson);
    }

    /**
     * Nastvení externí GeoJSON vrstvy.
     * @param code kód vrstvy
     * @param geoJson GeoJSON s objekty vrstvy
     * @param options volby nastavení
     */
    setExternalGeoJsonOverlay(code, geoJson, options) {
        this.vrstvyController.setExternalGeoJsonOverlay(code, geoJson, options);
    }


    // =========================================================================
    // Metody dispatchingEventListeneru pro obsluhu událostí z controllerů

    onLoadWmsProperties(url) {
        this.openPropertiesPanel();
        this.propertiesController.onLoadWmsProperties(url);
    }

    onShowProperties(properties) {
        this.openPropertiesPanel();
        this.propertiesController.onShowProperties(properties);
    }

    onShowPropertiesError(error) {
        this.openPropertiesPanel();
        this.propertiesController.onShowPropertiesError(error);
    }

}
