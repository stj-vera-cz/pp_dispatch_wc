
const WEBCOMPONENT_VERSION = "20.1.4";

console.log("pp_dispatch_wc version: " + WEBCOMPONENT_VERSION);
console.log("agent: " + navigator.userAgent);
console.log("obsolete gdc: " + Utils.isObsoleteGdc());

// defaulty pro jconfirm https://craftpip.github.io/jquery-confirm/#customizing
jconfirm.pluginDefaults.useBootstrap = false;
jconfirm.pluginDefaults.boxWidth = '400px';
jconfirm.pluginDefaults.animation = 'scale';

var dispatching = null;
var ppClient = null;

//==============================================================================
//== Genero gICAPI =============================================================
//==============================================================================

/**
 * Funkce nutná pro fungování webcomponenty z Genera.
 */
var onICHostReady = function(version) {
    if ( version != "1.0" ) {
        alert('Invalid API version');
    }
}

//==============================================================================
//== Funkce volané z Genera do JS ==============================================
//==============================================================================

/**
* Funkce volaná z Genera pro inicializaci mapové komponenty dispečinku.
* @optionsStr inicializační parametry dispečingu
*/
function genero_initDispatching(optionsStr) {
    console.log("genero_initDispatching:" + optionsStr);
    // inicializace mapy
    let options = JSON.parse(optionsStr);
    ppClient = new PPClient();
    dispatching = new PPDispatching("map", options, ppClient);
}

/**
* Funkce volaná z Genera pro nastavení vrstvy trasování strážníků z PP-M.
* @param geoJsonStr řetězec GeoJSONu s vrstvou trasování strážníků z PP-M
*/
function genero_setFeaturesTrasovaniPpm(geoJsonStr) {
    var geoJson = JSON.parse(geoJsonStr);
    dispatching.setFeaturesTrasovaniPpm(geoJson);
}

/**
* Funkce volaná z Genera pro nastavení vrstvy trasování strážníků z radiostanic.
* @param geoJsonStr řetězec GeoJSONu s vrstvou trasování strážníků z radiostanic
*/
function genero_setFeaturesTrasovaniRadio(geoJsonStr) {
    var geoJson = JSON.parse(geoJsonStr);
    dispatching.setFeaturesTrasovaniRadio(geoJson);
}

/**
* Funkce volaná z Genera pro nastavení vrstvy událostí.
* @param geoJsonStr řetězec GeoJSONu s vrstvou událostí
*/
function genero_setFeaturesUdalosti(geoJsonStr) {
    var geoJson = JSON.parse(geoJsonStr);
    dispatching.setFeaturesUdalosti(geoJson);
}

/**
* Funkce volaná z Genera pro nastavení vrstvy vyřešených událostí.
* @param geoJsonStr řetězec GeoJSONu s vrstvou událostí
*/
function genero_setFeaturesUdalostiVyresene(geoJsonStr) {
    var geoJson = JSON.parse(geoJsonStr);
    dispatching.setFeaturesUdalostiVyresene(geoJson);
}

/**
 * Funkce volaná z Genera pro nastavení obecné vrstvy markerů.
 * @param code kód vrstvy
 * @param geoJsonStr GeoJSON s vrstvou událostí
 */
function genero_setFeaturesMarkerLayer(code, geoJsonStr) {
    //console.log("genero_setFeaturesMarkerLayer:" + geoJsonStr);
    var geoJson = JSON.parse(geoJsonStr);
    dispatching.setFeaturesMarkerLayer(code, geoJson);
}

/**
* Funkce volaná z Genera pro nastavení externí GeoJSON vrstvy.
* @param code kód vrstvy
* @param geoJsonStr řetězec GeoJSONu
*/
function genero_setExternalGeoJsonOverlay(code, geoJsonStr, optionsStr) {
    var geoJson = JSON.parse(geoJsonStr);
    var optionsJson = optionsStr ? JSON.parse(optionsStr) : null;
    dispatching.setExternalGeoJsonOverlay(code, geoJson, optionsJson);
}

/**
* Funkce volaná z Genera pro nastavení výsledku akce.
* @param actionId id akce
* @param resultJsonStr řetězec JSONu s výsledkem akce
*/
function genero_actionResult(actionId, resultJsonStr) {
    var result = resultJsonStr ? JSON.parse(resultJsonStr): null;
    ppClient.actionResult(actionId, result);
}


//=============================================================================================
//== Pro testovací účely, voláno z parent HTML stránky kam je WC vložena do IFRAME ============
//=============================================================================================

// registrace message receiveru
// využití, pokud je webkomponenta vložená jako iframe do jiné html stránky, např. pro testování nebo komunikaci více webcomponent
window.addEventListener("message", receiveMessage, false);

function receiveMessage(event) {
    console.log(event.data.action);

    switch(event.data.action) {
        case 'initDispatching':
            genero_initDispatching(JSON.stringify(event.data.options));
            break;
        case 'setFeaturesTrasovaniPpm':
            genero_setFeaturesTrasovaniPpm(JSON.stringify(event.data.options));
            break;
        case 'setFeaturesTrasovaniRadio':
            genero_setFeaturesTrasovaniRadio(JSON.stringify(event.data.options));
            break;
        case 'setFeaturesUdalosti':
            genero_setFeaturesUdalosti(JSON.stringify(event.data.options));
            break;
        case 'setFeaturesUdalostiVyresene':
            genero_setFeaturesUdalostiVyresene(JSON.stringify(event.data.options));
            break;
        case 'setFeaturesMarkerLayer':
            genero_setFeaturesMarkerLayer(event.data.code, JSON.stringify(event.data.features));
            break;
        case 'setExternalGeoJsonOverlay':
            genero_setExternalGeoJsonOverlay(event.data.code, JSON.stringify(event.data.geoJson), JSON.stringify(event.data.options));
            break;
        default:
            console.log("Unknown action " + event.data.action);
    }
}
