
/**
 * Třída pro párování markerů, při přetažení jednoho na druhý.
 */
class MarkerPairer {

    constructor(map, layerProvider) {
        this.map = map;
        this.layerProvider = layerProvider;
    }

    /**
     * Vyhledá ze zadené vrstvy markery, které leží poblíž dané souřadnice.
     */
    pair(layerCode, latLng) {
        let _this = this;
        let match = [];

        if (this.layerProvider) {
            let layerDef = this.layerProvider.getActiveLayerByCode(layerCode);
            //console.log(layerDef.layer);
            if (layerDef && layerDef.layer && layerDef.layer._layers) {
                layerDef.layer.eachLayer(function(marker) {
                    if (marker._latlng) {
                        if (_this.matchesPosition(latLng, marker._latlng)) {
                            console.log("MATCH !!!");
                            console.log(marker._latlng);
                            match.push(marker);
                        }
                    }
                });
            }
        }

        return match;
    }

    matchesPosition(latLngA, latLngB) {
        let posA = this.map.latLngToLayerPoint(latLngA);
        let posB = this.map.latLngToLayerPoint(latLngB);

        return ((Math.abs(posA.x - posB.x) < 25) && (Math.abs(posA.y - posB.y) < 34));
    }
}
