
//debugger helper in the browser
//add mockup functions (not needed in production)
if (typeof(gICAPI) == "undefined") {
    gICAPI = {
        counter: 0,
        inputDataStr: null
    };
    gICAPI.SetData = function(dataStr) {
        console.log("SetData:" + dataStr);

        gICAPI.inputDataStr = dataStr;
    }
    gICAPI.SetFocus = function() {
        console.log("SetFocus");
    }
    gICAPI.Action = function(action) {
        console.log("Action:" + action);

        setTimeout(function(){
            console.log("send actionResult");
            let data = JSON.parse(gICAPI.inputDataStr);
            let dataResult = null;

            switch (action) {
                case 'identifikace':
                    dataResult = fakeResultIdentifikace(data);
                    break;
                case 'vyhledej_adresy':
                    dataResult = statusResult(fakeResultVyhledejAdresy1(data.input));
                    break;
                case 'prirad_udalost_hlidce':
                    dataResult = statusResult(fakeResultPriradUdalostHlidce(data));
                    break;
                case 'wms_get_feature_info':
                    dataResult = statusResult(fakeResultWmsGetFeatureInfo(data));
                    break;
                default:
                    dataResult = data;
            }

            ppClient.actionResult(data.actionId, dataResult);
        }, 500);
    }
}

function statusResult(dataResult) {
    //let status = gICAPI.counter++ % 2;
    let status = 0;
    return {status:status, data: dataResult, errmsg:"Fatal error!!!"}
}

function fakeResultIdentifikace(data) {
    return {"status":0,"data":{"isvr":{"status":0,"errmsg":null,"data":{"parId":"87654321","cislo":"123/4","cisloLV":"12345678","zpusobVyuziti":"pastva pro ovečky","druhPozemku":"louka"}},"iskn":{"status":0,"errmsg":null,"data":{"Kod":"87654321","Vlastnik":[{"FyzickaOsoba":{"Jmeno":"Andrea","TitulPredJmenem":"Ing.","TitulZaJmenem":"","ZkraceneRc":"945109","Id":"72036723010","Prijmeni":"Dymáková","Adresa":{"Psc":"53322","TypCislaDomovniho":"CISLO_POPISNE","Stat":"","Obec":"Mikulovice","Ulice":"Zahradní","CisloDomovni":"55","CastObce":"Mikulovice","CisloOrientacni":"","Okres":""}}},{"PravnickaOsoba":{"DoplnekIco":"","Nazev":"LARSEN Development a.s.","CharOSNazev":"","Ico":"24130427","Adresa":{"Psc":"19000","TypCislaDomovniho":"CISLO_POPISNE","Stat":"","Obec":"Praha","Ulice":"Poděbradská","CisloDomovni":"538","CastObce":"Vysočany","CisloOrientacni":"46"}}},{"SpolecneJmeniManzelu":{"Id":"57845688010","Nazev":"Novák Jan a Nováková Petra","Partner1":{"Jmeno":"Jan","TitulPredJmenem":"","TitulZaJmenem":"","ZkraceneRc":"911129","Id":"57785778010","Prijmeni":"Novák","Adresa":{"Psc":"53701","TypCislaDomovniho":"CISLO_POPISNE","Stat":"","Obec":"Chrudim","Ulice":"Šnajdrova","CisloDomovni":"999","CastObce":"Chrudim III","CisloOrientacni":"","Okres":""}},"Partner2":{"Jmeno":"Petra","TitulZaJmenem":"","ZkraceneRc":"935305","Id":"57785779010","Prijmeni":"Nováková","Adresa":{"Psc":"53701","TypCislaDomovniho":"CISLO_POPISNE","Stat":"","Obec":"Chrudim","Ulice":"Šnajdrova","CisloDomovni":"999","CastObce":"Chrudim III","CisloOrientacni":"","Okres":""}}}}]}},"geometry":{"status":0,"errmsg":null,"data":{"type":"FeatureCollection","features":[{"type":"Feature","properties":{"number":2},"geometry":{"type":"Polygon","coordinates":[[[15.460314,50.15389],[15.460617,50.1539],[15.460819,50.153408],[15.460193,50.1534],[15.460314,50.15389]]]}}]}}}};
}

/*
function fakeResultIdentifikace(data) {
    return {"isvr":{"status":0,"errmsg":"ISVR je absolutně out of order","data":{"parId":"87654321","cislo":"123/4","cisloLV":"12345678","zpusobVyuziti":"pastva pro ovečky","druhPozemku":"louka"}},"iskn":{"status":1,"errmsg":"ISKN vůbec nejede","data":null},"geometry":{"status":3,"errmsg":"Geometrie neni","data":{"type":"FeatureCollection","features":[{"type":"Feature","properties":{"number":2},"geometry":{"type":"Polygon","coordinates":[[[15.460314,50.15389],[15.460617,50.1539],[15.460819,50.153408],[15.460193,50.1534],[15.460314,50.15389]]]}}]}}};
}
*/

/*
function fakeResultIdentifikace(data) {
    return {};
}
*/

function fakeResultVyhledejAdresy1(input) {
    let adresy = {"adresy":[{"id":3001,"adresa":"Na Hatích 72, 50351 Chlumec nad Cidlinou","latLng":{"lat":50.15306,"lng":15.4659}},{"id":3002,"adresa":"Na Lukách 148, 50351 Chlumec nad Cidlinou","latLng":{"lat":50.15059,"lng":15.46549}},{"id":3003,"adresa":"Na Valech 98, 50351 Chlumec nad Cidlinou","latLng":{"lat":50.15552,"lng":15.46264}},{"id":3004,"adresa":"Na Vinici 197, 50351 Chlumec nad Cidlinou","latLng":{"lat":50.15635,"lng":15.45118}},{"id":3005,"adresa":"Nádražní č. p. 800, 50351 Chlumec nad Cidlinou","latLng":{"lat":50.16423,"lng":15.45427}}]};
    if (input.length > 2) {
        result = {};
        result.adresy = adresy.adresy.filter((adr) =>
            adr.adresa.toLowerCase().startsWith(input.toLowerCase())
        );
        return result;
    }

    return adresy;
}

function fakeResultVyhledejAdresy(input) {
    let adresy = {"adresy":[]};

    return adresy;
}

function fakeResultPriradUdalostHlidce(data) {
    return {
        "udalostFeature": {
           "type": "Feature",
           "geometry": {
               "type": "Point",
               "coordinates": [15.456576, 50.152768]
           },
           "properties": {
               "entityType":"udalost",
               "popupContent": "Přiřazená událost",
               "cisloUdalosti": "cislo/rok",
               "udalostId": data.udalostId,
               "pridelenoHlidky": [
                   {
                       "id":111,
                       "nazev":"Flákači"
                   }
               ]
           }
        }
    }
}


function fakeResultWmsGetFeatureInfo(data) {
    return {"info": "<!DOCTYPE html><html><head><title>Title of the document</title></head><body><h2>Hřiště Arnošta Popelky z Petrovic</h2><div>Výměra: 82ha</div></body></html>"};
}

/*
function fakeResultWmsGetFeatureInfo(data) {
    return _fakeWmsInfoResult;
}
*/
