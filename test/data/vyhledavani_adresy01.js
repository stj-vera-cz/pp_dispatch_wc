{
    "adresy": [
        {
            "id": 3001,
            "ulice": "Na Hatích 72",
            "latLng": {
                "lat": 50.15306, "lng": 15.4659
            }
        },
        {
            "id": 3002,
            "ulice": "Na Lukách 148",
            "latLng": {
                "lat": 50.15059,"lng": 15.46549
            }
        },
        {
            "id": 3003,
            "ulice": "Na Valech 98",
            "latLng": {
                "lat": 50.15552, "lng": 15.46264
            }
        },
        {
            "id": 3004,
            "ulice": "Na Vinici 197",
            "latLng": {
                "lat": 50.15635, "lng": 15.45118
            }
        },
        {
            "id": 3005,
            "ulice": "Nádražní 800",
            "latLng": {
                "lat": 50.16423, "lng": 15.45427
            }
        }
    ]
}
