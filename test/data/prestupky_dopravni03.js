var prestupkyDopravni03 = {
   "type": "FeatureCollection",
   "features": [
       {
          "type": "Feature",
          "geometry": {
              "type": "Point",
              "coordinates": [15.464176, 50.15708]
          },
          "properties": {
              "id": 1557,
              "entityType":"prestupek",
              "popupContent": "Přestupek 1557/2021<br />Crash update!!!",
              "beautify": {
                  "icon": "car-crash",
                  "iconShape": "marker",
                  "borderColor": "#CC3366",
                  "textColor": "#000000"
              }
          }
       },
       {
          "type": "Feature",
          "geometry": {
              "type": "Point",
              "coordinates": [15.464676, 50.15308]
          },
          "properties": {
              "id": 1558,
              "entityType":"prestupek",
              "popupContent": "Přestupek 1558/2021<br />Crash 2!!!",
              "beautify": {
                  "icon": "car-crash",
                  "iconShape": "marker",
                  "borderColor": "#CC3366",
                  "textColor": "#000000",
                  "iconSize": [40, 40],
                  "iconAnchor": [1, 0]
              }
          }
       }
    ]
};
