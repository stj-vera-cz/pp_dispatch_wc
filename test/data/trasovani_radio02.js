var trasovaniRadio02 = {
   "type": "FeatureCollection",
   "features": [{
       "type": "Feature",
       "geometry": {
           "type": "Point",
           "coordinates": [15.463576, 50.153168]
       },
       "properties": {
           "entityType":"trasovani_radio",
           "online": true,
           "radiostaniceId": 51,
           "popupContent": "0158",
           "straznikId": 11,
           "evidencniCislo": "666",
           "celeJmeno": "Josef Vyskočil",
           "hlidkaId": 111,
           "aktivniMajak": true,
           "udalosti": [
               {
                   "id": 1158,
                   "popis": "Koťátko na stromě",
                   "latLng": [50.155768, 15.460476],
                   "akceptovana": true
               },
               {
                   "id": 1159,
                   "popis": "Laco dělá bordel",
                   "latLng": [50.151768, 15.463476],
                   "akceptovana": false
               }
           ]
       }
   },
   {
       "type": "Feature",
       "geometry": {
           "type": "Point",
           "coordinates": [15.465576, 50.155768]
       },
       "properties": {
           "entityType":"trasovani_radio",
           "online": false,
           "radiostaniceId": 52,
           "popupContent": "2001",
           "straznikId": 12,
           "evidencniCislo": "007",
           "celeJmeno": "František Vokurka",
           "hlidkaId": 112,
           "aktivniMajak": true
       }
   },
   {
       "type": "Feature",
       "geometry": {
           "type": "Point",
           "coordinates": [15.465176, 50.154768]
       },
       "properties": {
           "entityType":"trasovani_radio",
           "online": true,
           "radiostaniceId": 53,
           "popupContent": "2002",
           "straznikId": 13,
           "evidencniCislo": "008",
           "celeJmeno": "Reinhard Maňásek",
           "hlidkaId": 113,
           "aktivniMajak": false
       }
   }]
};
