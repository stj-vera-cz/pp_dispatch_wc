var trasovaniPpm01 = {
   "type": "FeatureCollection",
   "features": [{
       "type": "Feature",
       "geometry": {
           "type": "Point",
           "coordinates": [15.460576, 50.153768]
       },
       "properties": {
           "entityType":"trasovani_ppm",
           "popupContent": null,
           "online": true,
           "iconClass": "dog",
           "straznikId": 11,
           "evidencniCislo": "666",
           "celeJmeno": "Josef Vyskočil",
           "hlidkaId": 111
       }
   },
   {
       "type": "Feature",
       "geometry": {
           "type": "Point",
           "coordinates": [15.462576, 50.154768]
       },
       "properties": {
           "entityType":"trasovani_ppm",
           "popupContent": "Strážník Franta",
           "online": true,
           "straznikId": 12,
           "evidencniCislo": "007",
           "celeJmeno": "František Vokurka",
           "hlidkaId": 112
       }
   },
   {
       "type": "Feature",
       "geometry": {
           "type": "Point",
           "coordinates": [15.45206, 50.15955]
       },
       "properties": {
           "entityType":"trasovani_ppm",
           "popupContent": null,
           "online": false,
           "straznikId": 13,
           "evidencniCislo": "000",
           "celeJmeno": "Vendelín Popelka",
           "hlidkaId": 112
       }
   }]
};
