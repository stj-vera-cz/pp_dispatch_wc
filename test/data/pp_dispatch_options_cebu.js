const ppDispatchOptions = {
    map: {
        center: [48.9752032, 14.4886494],
        zoom: 16
    },
    layers: {
        baseLayers: [
            "osm",
            "osm_bw",
            "cuzk_orto"
        ],
        overlays: [
            "trasovani_ppm",
            "trasovani_radio",
            "trasovani_radio_auto",
            "udalosti"
        ],
        wmsLayers: [
            {
				active: false,
				name: "ČÚZK parcely",
                url: "http://wms.cuzk.cz/wms.asp",
                options: {
                    layers : 'RST_KN,RST_KMD,parcelni_cisla,hranice_parcel,omp',
                    format : 'image/png',
                    transparent : true,
                    version : '1.3.0',
                    maxZoom : 19
                }
			},
            {
				active: false,
				name: "ČÚZK ostatní",
                url: "http://wms.cuzk.cz/wms.asp",
                options: {
                    layers : 'polygony_budov,dalsi_p_mapy',
                    format : 'image/png',
                    transparent : true,
                    version : '1.3.0',
                    maxZoom : 19
                }
			},
            {
                code: "ZABAGEDsilnice",
				active: false,
				name: "ZABAGED silnice s dloooouhatáááánským názvem",
                url: "https://ags.cuzk.cz/arcgis/services/ZABAGED/MapServer/WmsServer?",
                options: {
                    layers : '98,99,100',
					transparent: true,
					format: 'image/png'
                }
			},
            {
                code: "ZABAGEDbudovy",
                active: true,
                name: "ZABAGED budovy",
                url: "https://ags.cuzk.cz/arcgis/services/ZABAGED/MapServer/WmsServer?",
                options: {
                    layers : '62,42,43,37',
					transparent: true,
					format: 'image/png'
                }
            },
			{
                code: "CEBU",
                active: true,
                name: "CEBU",
                url: "http://vmappgis04.c-budejovice.cz/wms_pas/service.svc/get?",
                options: {
                    layers : 'L5',
					transparent: true,
					format: 'image/png'
                }
            },
			{
                code: "CEBUrozvody",
                active: true,
                name: "CEBU rozvody",
                url: "http://vmappgis04.c-budejovice.cz/wms_tema/service.svc/get?",
                options: {
                    layers : 'L16',
					transparent: true,
					format: 'image/png'
                }
            }
        ]
    }
};
