var identifikace01 =
{
    "status": 0,
    "data":
    {
       "isvr": {
          "status": 0,
          "errmsg": null,
          "data": {
             "parId": "87654321",
             "cislo": "123/4",
             "cisloLV": "12345678",
             "zpusobVyuziti": "pastva pro ovečky",
             "druhPozemku": "louka"
          }
       },
       "iskn": {
          "status": 0,
          "errmsg": null,
          "data": {
             "Kod": "87654321",
             "Vlastnik": [
                {
                   "FyzickaOsoba": {
                      "Jmeno": "Andrea",
                      "TitulPredJmenem": "Ing.",
                      "TitulZaJmenem": "",
                      "ZkraceneRc": "945109",
                      "Id": "72036723010",
                      "Prijmeni": "Dymáková",
                      "Adresa": {
                         "Psc": "53322",
                         "TypCislaDomovniho": "CISLO_POPISNE",
                         "Stat": "",
                         "Obec": "Mikulovice",
                         "Ulice": "Zahradní",
                         "CisloDomovni": "55",
                         "CastObce": "Mikulovice",
                         "CisloOrientacni": "",
                         "Okres": ""
                      }
                   }
                },
                {
                   "PravnickaOsoba": {
                      "DoplnekIco": "",
                      "Nazev": "LARSEN Development a.s.",
                      "CharOSNazev": "",
                      "Ico": "24130427",
                      "Adresa": {
                         "Psc": "19000",
                         "TypCislaDomovniho": "CISLO_POPISNE",
                         "Stat": "",
                         "Obec": "Praha",
                         "Ulice": "Poděbradská",
                         "CisloDomovni": "538",
                         "CastObce": "Vysočany",
                         "CisloOrientacni": "46"
                      }
                   }
                },
                {
                   "SpolecneJmeniManzelu": {
                      "Id": "57845688010",
                      "Nazev": "Novák Jan a Nováková Petra",
                      "Partner1": {
                         "Jmeno": "Jan",
                         "TitulPredJmenem": "",
                         "TitulZaJmenem": "",
                         "ZkraceneRc": "911129",
                         "Id": "57785778010",
                         "Prijmeni": "Novák",
                         "Adresa": {
                            "Psc": "53701",
                            "TypCislaDomovniho": "CISLO_POPISNE",
                            "Stat": "",
                            "Obec": "Chrudim",
                            "Ulice": "Šnajdrova",
                            "CisloDomovni": "999",
                            "CastObce": "Chrudim III",
                            "CisloOrientacni": "",
                            "Okres": ""
                         }
                      },
                      "Partner2": {
                         "Jmeno": "Petra",
                         "TitulZaJmenem": "",
                         "ZkraceneRc": "935305",
                         "Id": "57785779010",
                         "Prijmeni": "Nováková",
                         "Adresa": {
                            "Psc": "53701",
                            "TypCislaDomovniho": "CISLO_POPISNE",
                            "Stat": "",
                            "Obec": "Chrudim",
                            "Ulice": "Šnajdrova",
                            "CisloDomovni": "999",
                            "CastObce": "Chrudim III",
                            "CisloOrientacni": "",
                            "Okres": ""
                         }
                      }
                   }
                }
             ]
          }
       },
       "geometry": {
          "status": 0,
          "errmsg": null,
          "data": {
             "type": "FeatureCollection",
             "features": [
                {
                   "type": "Feature",
                   "properties": {
                      "number": 2
                   },
                   "geometry": {
                      "type": "Polygon",
                      "coordinates": [
                         [
                            [
                               15.460314,
                               50.15389
                            ],
                            [
                               15.460617,
                               50.1539
                            ],
                            [
                               15.460819,
                               50.153408
                            ],
                            [
                               15.460193,
                               50.1534
                            ],
                            [
                               15.460314,
                               50.15389
                            ]
                         ]
                      ]
                   }
                }
             ]
          }
       }
    }
}
