var trasovaniRadio01 = {
   "type": "FeatureCollection",
   "features": [{
       "type": "Feature",
       "geometry": {
           "type": "Point",
           "coordinates": [15.463576, 50.153168]
       },
       "properties": {
           "entityType":"trasovani_radio",
           "online": true,
           "radiostaniceId": 51,
           "popupContent": "0158",
           "straznikId": 11,
           "evidencniCislo": "666",
           "celeJmeno": "Josef Vyskočil",
           "hlidkaId": 111,
           "aktivniMajak": false,
           "aktivniNouze": true
       }
   },
   {
       "type": "Feature",
       "geometry": {
           "type": "Point",
           "coordinates": [15.465576, 50.155768]
       },
       "properties": {
           "entityType":"trasovani_radio",
           "online": true,
           "radiostaniceId": 52,
           "popupContent": "2001",
           "straznikId": 12,
           "evidencniCislo": "007",
           "celeJmeno": "František Vokurka",
           "hlidkaId": 112,
           "aktivniMajak": false
       }
   },
   {
       "type": "Feature",
       "geometry": {
           "type": "Point",
           "coordinates": [15.465176, 50.154768]
       },
       "properties": {
           "entityType":"trasovani_radio",
           "radiostaniceId": 53,
           "popupContent": "2002",
           "straznikId": 13,
           "evidencniCislo": "008",
           "celeJmeno": "Reinhard Maňásek",
           "hlidkaId": 113,
           "aktivniMajak": true
       }
   }]
};
