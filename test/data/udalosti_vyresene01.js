var udalostiVyresene01 = {
   "type": "FeatureCollection",
   "features": [
        {
           "type": "Feature",
           "geometry": {
               "type": "Point",
               "coordinates": [15.461576, 50.15648]
           },
           "properties": {
               "entityType":"udalost",
               "popupContent": "758/2020<br />Ožrala ve škarpě",
               "cisloUdalosti": "758/2020/BLA",
               "udalostId": 758,
               "pridelenoHlidky": [
                   {
                       "id":111,
                       "nazev":"Flákači"
                   }
               ]
           }
        },
        {
           "type": "Feature",
           "geometry": {
               "type": "Point",
               "coordinates": [15.458576, 50.157768]
           },
           "properties": {
               "entityType":"udalost",
               "popupContent": "759/2020<br />Ideozločin - kobliha v popelnici",
               "cisloUdalosti": "759/2020/BLA",
               "udalostId": 759
           }
       },
       {
          "type": "Feature",
          "geometry": {
              "type": "Point",
              "coordinates": [15.462576, 50.154768]
          },
          "properties": {
              "entityType":"udalost",
              "popupContent": "760/2020<br />Dežo nafackoval Jožovi",
              "cisloUdalosti": "760/2020/BLA",
              "udalostId": 760
          }
       }
    ]
};
