const ppDispatchOptions = {
    map: {
        center: [50.153768, 15.460576],
        zoom: 15
    },
    search: {
        active: true
    },
    layers: {
        baseLayers: [
            "osm",
            "osm_bw",
            "cuzk_orto",
            "empty"
        ],
        overlays: [
            {
                "code": "trasovani_ppm",
                "active": false
            },
            {
                "code": "trasovani_radio",
                "active": false
            },
            "udalosti",
            {
                "code": "udalosti_vyresene"
            },
            {
                "code": "prestupky_dopravni",
                "name": "Dopravní přestupky",
                "active": true,
                "layerType": "feature_marker"
            },
            {
                "code": "prestupky_tpzov",
                "name": "Přestupky s TPZOV",
                "active": true,
                "layerType": "feature_marker"
            }
        ],
        externalOverlays: [
            {
                code: "zabory",
                active: true,
				name: "Zábory",
                color: "#e69138"
            },
            {
                code: "brambory",
                active: true,
				name: "Brambory"
            }
        ],
        wmsLayers: [
            {
                code: "ZABAGEDsilnice",
                active: false,
                name: "[Kriz. řízení] ZABAGED silnice",
                url: "https://ags.cuzk.cz/arcgis/services/ZABAGED/MapServer/WmsServer?",
                options: {
                    layers : '98,99,100',
                    transparent: true,
                    format: 'image/png'
                }
            },
            {
				active: false,
                url: "http://wms.cuzk.cz/wms.asp",
                options: {
                    layers : 'RST_KN,RST_KMD,parcelni_cisla,hranice_parcel,omp',
                    format : 'image/png',
                    transparent : true,
                    version : '1.3.0',
                    maxZoom : 19
                }
			},
            {
				active: false,
				name: "[Správní hranice] ČÚZK parcely",
                url: "http://wms.cuzk.cz/wms.asp",
                options: {
                    layers : 'RST_KN,RST_KMD,parcelni_cisla,hranice_parcel,omp',
                    format : 'image/png',
                    transparent : true,
                    version : '1.3.0',
                    maxZoom : 19
                }
			},
            {
				active: true,
				name: "[Správní hranice] ČÚZK ostatní",
                url: "http://wms.cuzk.cz/wms.asp",
                options: {
                    layers : 'polygony_budov,dalsi_p_mapy',
                    format : 'image/png',
                    transparent : true,
                    version : '1.3.0',
                    maxZoom : 19
                }
			},
            {
                active: true,
                name: "[Kriz. řízení] ZABAGED budovy",
                url: "https://ags.cuzk.cz/arcgis/services/ZABAGED/MapServer/WmsServer?",
                options: {
                    layers : '62,42,43,37',
                    transparent: true,
					format: 'image/png'
                }
            }
        ]
    }
};
