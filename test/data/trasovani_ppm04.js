var trasovaniPpm04 = {
   "type": "FeatureCollection",
   "features": [{
       "type": "Feature",
       "geometry": {
           "type": "Point",
           "coordinates": [15.462776, 50.154868]
       },
       "properties": {
           "entityType":"trasovani_ppm",
           "popupContent": null,
           "online": false,
           "iconClass": "dog",
           "straznikId": 11,
           "evidencniCislo": "666",
           "celeJmeno": "Josef Vyskočil",
           "hlidkaId": 111,
           "udalosti": [
               {
                   "id": 1158,
                   "popis": "Koťátko na stromě",
                   "latLng": [50.155768, 15.460476],
                   "akceptovana": true
               },
               {
                   "id": 1159,
                   "popis": "Laco dělá bordel",
                   "latLng": [50.151768, 15.463476],
                   "akceptovana": false
               }
           ]
       }
   },
   {
       "type": "Feature",
       "geometry": {
           "type": "Point",
           "coordinates": [15.45626, 50.15835]
       },
       "properties": {
           "entityType":"trasovani_ppm",
           "popupContent": null,
           "online": true,
           "straznikId": 13,
           "evidencniCislo": "000",
           "celeJmeno": "Vendelín Popelka",
           "hlidkaId": 112,
           "udalosti": [
           ]
       }
   },
   {
       "type": "Feature",
       "geometry": {
           "type": "Point",
           "coordinates": [15.462976, 50.153568]
       },
       "properties": {
           "entityType":"trasovani_ppm",
           "popupContent": null,
           "online": true,
           "straznikId": 14,
           "evidencniCislo": "114",
           "celeJmeno": "Špelec Anton",
           "hlidkaId": 112,
           "udalosti": [
           ]
       }
   }
]
};
