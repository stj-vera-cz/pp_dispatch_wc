var udalosti02 = {
   "type": "FeatureCollection",
   "features": [
        {
           "type": "Feature",
           "geometry": {
               "type": "Point",
               "coordinates": [15.460576, 50.156768]
           },
           "properties": {
               "entityType":"udalost",
               "popupContent": "Hoří",
               "cisloUdalosti": "158/2020/BLA",
               "udalostId": 1158,
               "prideleno": true
           }
        },
        {
          "type": "Feature",
          "geometry": {
              "type": "Point",
              "coordinates": [15.464576, 50.150668]
          },
          "properties": {
              "entityType":"udalost",
              "popupContent": "Dežo mlátí Eržiku",
              "cisloUdalosti": "160/2020/BLA",
              "udalostId": 1160,
              "prideleno": null
          }
       },
       {
          "type": "Feature",
          "geometry": {
              "type": "Point",
              "coordinates": [15.463576, 50.150268]
          },
          "properties": {
              "entityType":"udalost",
              "popupContent": "Squateři na střeše",
              "cisloUdalosti": "161/2020/BLA",
              "udalostId": 1161,
              "prideleno": false
          }
      },
      {
         "type": "Feature",
         "geometry": {
             "type": "Point",
             "coordinates": [15.455576, 50.156868]
         },
         "properties": {
             "entityType":"udalost",
             "popupContent": "Prská koronavirus na lidi",
             "cisloUdalosti": "162/2020/BLA",
             "udalostId": 1162,
             "prideleno": true
         }
     }
    ]
};
