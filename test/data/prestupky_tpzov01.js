var prestupkyTpzov01 = {
   "type": "FeatureCollection",
   "features": [
        {
           "type": "Feature",
           "geometry": {
               "type": "Point",
               "coordinates": [15.462576, 50.15808]
           },
           "properties": {
               "id": 1546,
               "entityType":"prestupek",
               "popupContent": "Přestupek 1546/2021<br />Bota Adidas Superstar II",
               "beautify": {
                   "icon": "shoe-prints",
                   "iconShape": "marker",
                   "borderColor": "#AA3300",
                   "textColor": "#000000"
               }
           }
       },
       {
          "type": "Feature",
          "geometry": {
              "type": "Point",
              "coordinates": [15.463576, 50.15508]
          },
          "properties": {
              "id": 1547,
              "entityType":"prestupek",
              "popupContent": "Přestupek 1547/2021<br />Bota Baťa",
              "beautify": {
                  "icon": "shoe-prints",
                  "iconShape": "marker",
                  "borderColor": "#AA3300",
                  "textColor": "#000000"
              }
          }
       }
    ]
};
