var udalostiVyresene02 = {
   "type": "FeatureCollection",
   "features": [
        {
           "type": "Feature",
           "geometry": {
               "type": "Point",
               "coordinates": [15.461576, 50.15648]
           },
           "properties": {
               "entityType":"udalost",
               "popupContent": "758/2020<br />Ožrala ve škarpě",
               "cisloUdalosti": "758/2020/BLA",
               "udalostId": 758
           }
        },
        {
           "type": "Feature",
           "geometry": {
               "type": "Point",
               "coordinates": [15.458576, 50.157768]
           },
           "properties": {
               "entityType":"udalost",
               "popupContent": "759/2020<br />Ideozločin - kobliha v popelnici",
               "cisloUdalosti": "759/2020/BLA",
               "udalostId": 759
           }
       },
      {
         "type": "Feature",
         "geometry": {
             "type": "Point",
             "coordinates": [15.45976, 50.159768]
         },
         "properties": {
             "entityType":"udalost",
             "popupContent": "761/2020<br />Kuna v kurníku",
             "cisloUdalosti": "761/2020/BLA",
             "udalostId": 761
         }
     },
     {
        "type": "Feature",
        "geometry": {
            "type": "Point",
            "coordinates": [15.463576, 50.152768]
        },
        "properties": {
            "entityType":"udalost",
            "popupContent": "762/2020<br />Dežo nadává do dylin",
            "cisloUdalosti": "762/2020/BLA",
            "udalostId": 762
        }
    }
    ]
};
