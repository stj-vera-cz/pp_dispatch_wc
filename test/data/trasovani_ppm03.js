var trasovaniPpm03 = {
   "type": "FeatureCollection",
   "features": [{
       "type": "Feature",
       "geometry": {
           "type": "Point",
           "coordinates": [15.462576, 50.154768]
       },
       "properties": {
           "entityType":"trasovani_ppm",
           "online": true,
           "popupContent": null,
           "iconClass": "dog",
           "straznikId": 11,
           "evidencniCislo": "666",
           "celeJmeno": "Josef Vyskočil",
           "hlidkaId": 111
       }
   },
   {
       "type": "Feature",
       "geometry": {
           "type": "Point",
           "coordinates": [15.45226, 50.15855]
       },
       "properties": {
           "entityType":"trasovani_ppm",
           "popupContent": null,
           "straznikId": 13,
           "evidencniCislo": "000",
           "celeJmeno": "Vendelín Popelka",
           "hlidkaId": 112
       }
   }]
};
