var trasovaniPpm02 = {
   "type": "FeatureCollection",
   "features": [{
       "type": "Feature",
       "geometry": {
           "type": "Point",
           "coordinates": [15.464476, 50.154968]
       },
       "properties": {
           "entityType":"trasovani_ppm",
           "popupContent": null,
           "iconClass": "dog",
           "online": true,
           "straznikId": 11,
           "evidencniCislo": "666",
           "celeJmeno": "Josef Vyskočil",
           "hlidkaId": 111
       }
   },
   {
       "type": "Feature",
       "geometry": {
           "type": "Point",
           "coordinates": [15.462876, 50.153768]
       },
       "properties": {
           "entityType":"trasovani_ppm",
           "online": false,
           "popupContent": "007 Strážník Franta<br />hlídka X<br />událost1<br />událost2<br />událost3<br />událost4<br />událost5<br />událost6",
           "straznikId": 12,
           "evidencniCislo": "007",
           "celeJmeno": "František Vokurka",
           "hlidkaId": 112
       }
   },
   {
       "type": "Feature",
       "geometry": {
           "type": "Point",
           "coordinates": [15.45226, 50.15855]
       },
       "properties": {
           "entityType":"trasovani_ppm",
           "online": true,
           "iconClass": "fas fa-angry",
           "popupContent": null,
           "straznikId": 13,
           "evidencniCislo": "000",
           "celeJmeno": "Vendelín Popelka",
           "hlidkaId": 112
       }
   }]
};
