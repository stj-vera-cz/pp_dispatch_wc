var prestupkyDopravni01 = {
   "type": "FeatureCollection",
   "features": [
        {
           "type": "Feature",
           "geometry": {
               "type": "Point",
               "coordinates": [15.464576, 50.15608]
           },
           "properties": {
               "id": 1556,
               "entityType":"prestupek",
               "popupContent": "Přestupek 1556/2021<br />Parking na hovado",
               "beautify": {
                   "icon": "angry",
                   "iconShape": "marker",
                   "borderColor": "#AA3300",
                   "textColor": "#000000"
               },
               "contextMenu":{
                    "contextmenu": true,
                    "contextmenuWidth": 140,
                    "contextmenuItems":
                        [
                            {
                                "index": 0,
                                "text": "Detail",
                                "iconCls": "fas fa-search",
                                "action": "prestupek_detail"
                            }
                        ]
                }
           }
       },
       {
          "type": "Feature",
          "geometry": {
              "type": "Point",
              "coordinates": [15.464176, 50.15708]
          },
          "properties": {
              "id": 1557,
              "entityType":"prestupek",
              "popupContent": "Přestupek 1557/2021<br />Crash!!!",
              "beautify": {
                  "icon": "car-crash",
                  "iconShape": "marker",
                  "borderColor": "#AA3300",
                  "textColor": "#000000"
              }
          }
       }
    ]
};
