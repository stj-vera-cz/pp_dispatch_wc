var external02 = {
   "type": "FeatureCollection",
   "features": [
        {
           "type": "Feature",
           "geometry": {
               "type": "Point",
               "coordinates": [15.460176, 50.146768]
           },
           "properties": {
               "entityType":"udalost",
               "popupContent": "Hoří",
               "tooltip": "Hoří",
               "udalostId": 1158,
               "pridelenoHlidky": [
                   {
                       "id":111,
                       "nazev":"Flákači"
                   }
               ]
           }
        },
        {
           "type": "Feature",
           "geometry": {
               "type": "Point",
               "coordinates": [15.453576, 50.156768]
           },
           "properties": {
               "entityType":"udalost",
               "popupContent": "Grilovačka bez roušek",
               "tooltip": "Grilovačka bez roušek. Řešeno na místě pendrekem, dvaceti ranami na holou.",
               "udalostId": 1159
           }
       },
       {
          "type": "Feature",
          "geometry": {
              "type": "Point",
              "coordinates": [15.464576, 50.152768]
          },
          "properties": {
              "entityType":"udalost",
              "popupContent": "Dežo mlátí Eržiku",
              "tooltip": "Dežo mlátí Eržiku",
              "udalostId": 1160
          }
      },
      {
         "type": "Feature",
         "geometry": {
             "type": "Point",
             "coordinates": [15.474576, 50.155768]
         },
         "properties": {
         }
      }
    ]
};
