var udalosti01 = {
   "type": "FeatureCollection",
   "features": [
        {
           "type": "Feature",
           "geometry": {
               "type": "Point",
               "coordinates": [15.460576, 50.156768]
           },
           "properties": {
               "entityType":"udalost",
               "popupContent": "Hoří",
               "cisloUdalosti": "158/2020/BLA",
               "udalostId": 1158,
               "prideleno": true
           }
        },
        {
           "type": "Feature",
           "geometry": {
               "type": "Point",
               "coordinates": [15.456576, 50.152768]
           },
           "properties": {
               "entityType":"udalost",
               "popupContent": "Grilovačka bez roušek",
               "cisloUdalosti": "159/2020/BLA",
               "udalostId": 1159,
                "prideleno": false
           }
       },
       {
          "type": "Feature",
          "geometry": {
              "type": "Point",
              "coordinates": [15.467576, 50.150768]
          },
          "properties": {
              "entityType":"udalost",
              "popupContent": "Dežo mlátí Eržiku",
              "cisloUdalosti": "160/2020/BLA",
              "udalostId": 1160,
               "prideleno": null
          }
       }
    ]
};
