var identifyVeraResult = {
    "status": 0,
    "data": {
        "info": {
            "vlastnik": "JUDr. Řeháček"
        },
        "geometry": {
            "type": "FeatureCollection",
            "features": [
                {
                    "type": "Feature",
                    "properties": {
                        "number": 2
                    },
                    "geometry": {
                        "type": "Polygon",
                        "coordinates": [
                            [
                                [
                                    15.460314,
                                    50.153888
                                ],
                                [
                                    15.460617,
                                    50.153899
                                ],
                                [
                                    15.460819,
                                    50.153407
                                ],
                                [
                                    15.460193,
                                    50.153402
                                ],
                                [
                                    15.460314,
                                    50.153888
                                ]
                            ]
                        ]
                    }
                }
            ]
        }
    }
}
